BigFix Dashboard is a project designed before v9... It has a backend service that gets installed and interacts directly with web reports and the server itself to pass information out as xml files.  These xml files are then consumed by various utilities... initially we had an iphone app as well as a Windows Desktop app.  

This project also integrated with Leftronic.com dashboards.