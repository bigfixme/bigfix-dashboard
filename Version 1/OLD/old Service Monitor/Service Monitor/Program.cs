﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Management;
using System.Net.Mail;
using System.ComponentModel;
using System.Net;
using System.Diagnostics;

//http://stackoverflow.com/questions/6711726/c-sharp-query-windows-service


namespace Service_Monitor
{
    class Program
    {
        static void Main(string[] args)
        {
            bool Debug = false;

            var parser = new CommandLineParser();
            string Server = parser.GetValue<string>("Server", args, "vbigfix81");
            //if (Debug) Server = "";

            string Service = parser.GetValue<string>("Service", args, "FillDB");
            //if (Debug) Service = "";

            string Status = parser.GetValue<string>("Status", args, "Stopped");


            string Exe = parser.GetValue<string>("Exe", args, "");
            string Args = parser.GetValue<string>("Args", args, "");


            string EmailTo = parser.GetValue<string>("EmailTo", args, "");
            if (Debug) EmailTo = "danielheth@hotmail.com";
            
            string EmailFrom = parser.GetValue<string>("EmailFrom", args, EmailTo);
            if (Debug) EmailFrom = "alerts@bigfix.moranit.com";

            string EmailSubject = parser.GetValue<string>("EmailSubject", args, "ALERT:  " + Server + "->" + Service + " has stopped!");
            string EmailBody = parser.GetValue<string>("EmailBody", args, "ALERT:  " + Server + "->" + Service + " has stopped!");
            

            string SMTP = parser.GetValue<string>("SMTP", args, "");
            if (Debug) SMTP = "mail.moranit.local";

            string SMTPUser = parser.GetValue<string>("SMTPUser", args, "");
            if (Debug) SMTPUser = "FillDB";

            string SMTPPass = parser.GetValue<string>("SMTPPass", args, "");
            if (Debug) SMTPPass = "BASEg8kAn";





            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine(" DanielHeth's Service Monitor v" + Assembly.GetExecutingAssembly().GetName().Version.ToString());
            Console.WriteLine("   For more information visit:  http://danielheth.com/programs");
            Console.WriteLine("-------------------------------------------------------------------------------" + Environment.NewLine);


            ManagementClass class1 = new ManagementClass(@"\\" + Server + "\\root\\cimv2:Win32_Service");

            foreach (ManagementObject ob in class1.GetInstances())
            {
                var name = ob.GetPropertyValue("Name");

                if (name.ToString().ToLower() == Service.ToLower())
                {

                    var machine = Server;
                    var displayName = ob.GetPropertyValue("Description");
                    var startMode = ob.GetPropertyValue("StartMode");
                    var status = ob.GetPropertyValue("State");

                    Console.WriteLine("machine = " + machine);
                    Console.WriteLine("displayName = " + displayName);
                    Console.WriteLine("name = " + name);
                    Console.WriteLine("startMode = " + startMode);
                    Console.WriteLine("status = " + status);


                    if (status.ToString().ToLower() != Status.ToLower())
                    {
                        //execute a program if desired
                        if (!string.IsNullOrEmpty(Exe))
                        {
                            Process exe = new Process();
                            exe.StartInfo.FileName = Exe;
                            exe.StartInfo.Arguments = Args;

                            exe.Start();
                        }


                        //email if not running
                        if (!string.IsNullOrEmpty(EmailTo))
                        {
                            // Command line argument must the the SMTP host.
                            SmtpClient client = new SmtpClient(SMTP);
                            client.EnableSsl = false;
                            if (SMTPUser == "")
                            {
                                client.UseDefaultCredentials = true;
                            }
                            else
                            {
                                client.UseDefaultCredentials = false;
                                client.Credentials = new NetworkCredential(SMTPUser, SMTPPass);
                            }


                            // Specify the e-mail sender.
                            // Create a mailing address that includes a UTF8 character
                            // in the display name.
                            MailAddress from = new MailAddress(EmailFrom, EmailFrom, System.Text.Encoding.UTF8);

                            // Set destinations for the e-mail message.
                            MailAddress to = new MailAddress(EmailTo);

                            // Specify the message content.
                            MailMessage message = new MailMessage(from, to);
                            message.Body = EmailBody;

                            
                            // Include some non-ASCII characters in body and subject.
                            message.Subject = EmailSubject;
                            message.SubjectEncoding = System.Text.Encoding.UTF8;

                            message.Body += Environment.NewLine;
                            message.BodyEncoding = System.Text.Encoding.UTF8;
                            
                            // Set the method that is called back when the send operation ends.
                            client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                            //// The userState can be any object that allows your callback 
                            //// method to identify this send operation.
                            //// For this example, the userToken is a string constant.
                            //string userState = "Alert Message";
                            //client.SendAsync(message, userState);
                            client.Send(message);

                            Console.WriteLine("Sending message... press c to cancel mail. Press any other key to exit.");
                            //string answer = Console.ReadLine();

                            //// If the user canceled the send, and mail hasn't been sent yet,
                            //// then cancel the pending operation.
                            //if (answer.StartsWith("c") && mailSent == false)
                            //{
                            //    client.SendAsyncCancel();
                            //}

                            // Clean up.
                            message.Dispose();
                            
                        }
                    }
                    break;  //we're only looking for this one service, so break out when we found it
                }
            }


        }











        static bool mailSent = false;
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            mailSent = true;
        }



    }
}
