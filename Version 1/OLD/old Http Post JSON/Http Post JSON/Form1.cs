﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;
using System.IO;

namespace Http_Post_JSON
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void JSON_Post(string JSON)
        {
            // Create a request using a URL that can receive a post.             
            WebRequest request = WebRequest.Create("https://www.leftronic.com/customSend/");

            // Set the Method property of the request to POST.            
            request.Method = "POST";

            // Create POST data and convert it to a byte array.            
            //WRITE JSON DATA TO VARIABLE D            
            string postData = JSON; // "{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"Infrastructure Number\", \"point\": " + textBox1.Text + " }";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.            
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.            
            request.ContentLength = byteArray.Length;
            // Get the request stream.            
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.            
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.            
            dataStream.Close();

            // Get the response.            
            WebResponse response = request.GetResponse();
            // Display the status.//            
            MessageBox.Show(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.            
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.            
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.           
            string responseFromServer = reader.ReadToEnd();
            // Display the content.           
            MessageBox.Show(responseFromServer);
            // Clean up the streams.           
            reader.Close();
            dataStream.Close();
            response.Close();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"Infrastructure Number\", \"point\": " + textBox1.Text + " }");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"stoplight\", \"point\": " + textBox2.Text + " }");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"sparkline\", \"point\": " + textBox3.Text + " }");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"linegraph\", \"point\": " + textBox4.Text + " }");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"dial\", \"point\": " + textBox5.Text + " }");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"horizontalbar\", \"point\": " + textBox6.Text + " }");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"verticalbar\", \"point\": " + textBox7.Text + " }");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"worldmap\", \"point\": [{ \"latitude\": " + textBox8.Text + " , \"longitude\": " + textBox9.Text + ", \"color\": \"" + comboBox1.Text + "\" }] }");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            JSON_Post("{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"usamap\", \"point\": [{ \"latitude\": " + textBox10.Text + " , \"longitude\": " + textBox11.Text + ", \"color\": \"" + comboBox2.Text + "\" }] }");
        }




    }
}
