﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using System.IO;

namespace ServiceCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            string arg = String.Join(" ", args);
            args = arg.Split('/');

            string outputFile = "";
            string ServiceName = "";
            string ServerName = "";

            foreach (string a in args)
            {
                string[] param = a.Split(new char[] { ' ' }, 2);
                switch (param[0].ToLower())
                {
                    case "guid": outputFile = "./" + param[1].Trim(); break;
                    case "service": ServiceName = param[1].Trim(); break;
                    case "server": ServerName = param[1].Trim(); break;
                }
            }


            
            ServiceController sc = new ServiceController(ServiceName, ServerName);

            string status = "";

            try
            {
                status = sc.Status.ToString();
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }

            if (File.Exists(outputFile)) File.Delete(outputFile);

            using (StreamWriter outfile = new StreamWriter(outputFile))
            {
                try
                {
                    outfile.Write(status);
                }
                catch (Exception ex)
                {
                    outfile.Write(ex.Message);
                }
            }

        }
    }
}
