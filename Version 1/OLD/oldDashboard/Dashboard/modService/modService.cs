﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Dashboard.ServiceModule
{
    public partial class modService : Module
    {
        public override string Title { get { return label1.Text; } set { label1.Text = value; } }
        
        public string ServiceName { get; set; }

        public string ServerName { get; set; }


        public modService()
        {
            InitializeComponent();
        }

        private void removeModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseModule(this, null);
        }


        #region "Moving Module"
        void label1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                base.modMouseDown(sender, e);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                contextMenuStrip1.Show(new Point(this.Parent.Left + this.Left + e.X, this.Parent.Top + this.Top + e.Y));
            }
        }

        void label1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseUp(sender, e);
        }

        void label1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseMove(sender, e);
        }
        #endregion



        public override string ExportModule()
        {
            string xml = "  <service>" + Environment.NewLine;

            xml += base.ExportModule();

            xml += "    <servicename>" + ServiceName + "</servicename>" + Environment.NewLine;
            xml += "    <servername>" + ServerName + "</servername>" + Environment.NewLine;

            xml += "  </service>" + Environment.NewLine + Environment.NewLine;
            return xml;
        }


        public override void ConfigureProperty(string PropertyName, string value)
        {
            switch (PropertyName)  //Module - generic properties
            {
                case "servicename":
                    this.ServiceName = value;
                    break;
                case "servername":
                    this.ServerName = value;
                    break;

                default:
                    base.ConfigureProperty(PropertyName, value);
                    break;
            }
        }


        int CurrentInterval = 0; string guid = "";
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (guid != "") //check for results file
            {
                if (File.Exists("./" + guid))
                {
                    System.IO.StreamReader myFile = new System.IO.StreamReader("./" + guid);
                    string results = myFile.ReadToEnd();
                    myFile.Close();

                    timer1.Interval = 60000; //return to the 1min wait time
                    File.Delete("./" + guid);
                    guid = "";

                    switch (results.Trim())
                    {
                        case "Running": this.BackColor = Color.Lime; break;
                        case "StartPending": this.BackColor = Color.Yellow; break;
                        case "ContinuePending":
                        case "Paused":
                        case "PausePending":
                        case "Stopped":
                        case "StopPending":
                            this.BackColor = Color.Red;
                            break;
                    }                    
                }
                else
                {
                    //keep waiting //setup a timeout in here to resume normal operations
                }

            }
            else
            {

                if (CurrentInterval <= 0)
                {
                    try
                    {
                        guid = Guid.NewGuid().ToString();

                        Process p = new Process();
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        startInfo.FileName = "cmd.exe";
                        startInfo.Arguments = "/C servicecheck.exe /guid " + guid + " /service " + ServiceName + " /server " + ServerName;
                        p.StartInfo = startInfo; 
                        p.Start();

                        timer1.Interval = 10000; //speed up timer
                    }
                    catch 
                    {
                        //this.BackColor = Color.Orange;
                    }

                    CurrentInterval = Frequency;
                }
                else
                {
                    CurrentInterval--;
                }



            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmService s = new frmService(((frmMain)this.Parent));
            
            s.PreloadValues(this);

            s.ShowDialog();
        }


    }
}
