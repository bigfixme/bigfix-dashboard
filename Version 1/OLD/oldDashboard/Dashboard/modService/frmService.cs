﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Dashboard.ServiceModule
{
    public partial class frmService : Form
    {
        frmMain fm;
        public frmService(frmMain fmain)
        {
            fm = fmain;
            InitializeComponent();
        }




        modService MyService = null;
        public void PreloadValues(modService mod)
        {
            MyService = mod;

            textTitle.Text = mod.Title;
            textFrequency.Text = mod.Frequency.ToString();
            trackBar1.Value = mod.Frequency;
            textServiceName.Text = mod.ServiceName;
            textServerName.Text = mod.ServerName;
        }





        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MyService != null)
            {
                MyService.Title = textTitle.Text;
                MyService.Frequency = trackBar1.Value;
                MyService.ServiceName = textServiceName.Text;
                MyService.ServerName = textServerName.Text;
            }
            else
            {

                modService m = new modService();
                m.OnCloseModule += new modService.NewCloseModuleDelegate(fm.Module_Close);
                m.OnMoveModule += new modService.NewMoveModuleDelegate(fm.Module_Move);

                m.Title = textTitle.Text;
                m.Frequency = trackBar1.Value;
                m.ServiceName = textServiceName.Text;
                m.ServerName = textServerName.Text;
                m.Top = 45;
                m.Width = 143;
                m.Height = 26;

                fm.Controls.Add(m);
                fm.Controls.SetChildIndex(m, 0);
                m.Visible = true;
            }

            fm.ProfileChanged = true;
            this.Close();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            textFrequency.Text = trackBar1.Value.ToString();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(textFrequency.Text, out val);

            trackBar1.Value = val;
        }
    }
}
