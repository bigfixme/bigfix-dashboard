﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Web.Services.Description;
using System.CodeDom;
using System.CodeDom.Compiler;

namespace Dashboard.BarModule
{
    public partial class modBar : Module
    {
        public override string Title { get { return label2.Text; } set { label2.Text = value; } }
        public string MinRelevance = "";
        public string Relevance = "";
        public string MaxRelevance = "";

        public string ServerName = "";
        public string PortNumber = "";
        public string Username = "";
        public string Password = "";


        public modBar()
        {
            InitializeComponent();
        }



        public override string ExportModule()
        {
            string xml = "  <bar>" + Environment.NewLine;

            xml += base.ExportModule();

            xml += "    <minrelevance>" + ((frmMain)this.Parent).EscapeString(MinRelevance) + "</minrelevance>" + Environment.NewLine;
            xml += "    <relevance>" + ((frmMain)this.Parent).EscapeString(Relevance) + "</relevance>" + Environment.NewLine;
            xml += "    <maxrelevance>" + ((frmMain)this.Parent).EscapeString(MaxRelevance) + "</maxrelevance>" + Environment.NewLine;

            xml += "    <servername>" + ((frmMain)this.Parent).EscapeString(ServerName) + "</servername>" + Environment.NewLine;
            xml += "    <portnumber>" + PortNumber + "</portnumber>" + Environment.NewLine;
            xml += "    <username>" + ((frmMain)this.Parent).EscapeString(Username) + "</username>" + Environment.NewLine;
            xml += "    <password>" + Password + "</password>" + Environment.NewLine;

            xml += "  </bar>" + Environment.NewLine + Environment.NewLine;
            return xml;
        }

        public override void ConfigureProperty(string PropertyName, string value)
        {
            switch (PropertyName)  //Module - generic properties
            {
                case "minrelevance": this.MinRelevance = value; break;
                case "relevance": this.Relevance = value; break;
                case "maxrelevance": this.MaxRelevance = value; break;

                case "servername": this.ServerName = value; break;
                case "portnumber": this.PortNumber = value; break;
                case "username": this.Username = value; break;
                case "password": this.Password = value; break;

                default:
                    base.ConfigureProperty(PropertyName, value);
                    break;
            }
        }





        #region "Moving Module"
        void Mod_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                base.modMouseDown(sender, e);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                contextMenuStrip1.Show(new Point(this.Parent.Left + this.Left + e.X, this.Parent.Top + this.Top + e.Y));
            }
        }

        void Mod_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseUp(sender, e);
        }

        void Mod_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseMove(sender, e);
        }
        #endregion










        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBar b = new frmBar(((frmMain)this.Parent));
            //b.LoadTemplates(((frmMain)this.Parent).Templates);

            b.PreloadValues(this);

            b.ShowDialog();
        }

        private void removeModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseModule(this, null);
        }

        private void modBar_Load(object sender, EventArgs e)
        {

        }














        vwebreports.RelevanceBinding bes = null;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ServerName != "" && Username != "" && Password != "")
            {
                //1. pull in minimum value, current value, and maximum value
                if (bes == null)
                {
                    bes = new vwebreports.RelevanceBinding(); ;
                    if (bes.Timeout != 10000) bes.Timeout = 10000;
                    if (bes.Url != "http://" + ServerName + ":" + PortNumber.ToString() + "/webreports?wsdl") bes.Url = "http://" + ServerName + ":" + PortNumber.ToString() + "/webreports?wsdl";
                }

                String[] res = null;

                int MinValue = 0;
                if (MinRelevance != "" && !int.TryParse(MinRelevance, out MinValue))
                {
                    try
                    {
                        res = bes.GetRelevanceResult(MinRelevance, Username, Password);
                    }
                    catch { }
                    MinValue = int.Parse(res[0]);
                }

                int CurValue = 0;
                if (Relevance != "" && !int.TryParse(Relevance, out CurValue))
                {
                    try
                    {
                        res = bes.GetRelevanceResult(Relevance, Username, Password);
                    }
                    catch { }
                    CurValue = int.Parse(res[0]);
                }

                int MaxValue = 100000;
                if (MaxRelevance != "" && !int.TryParse(MaxRelevance, out MaxValue))
                {
                    try
                    {
                        res = bes.GetRelevanceResult(MaxRelevance, Username, Password);
                    }
                    catch { }
                    MaxValue = int.Parse(res[0]);
                }



                label1.Text = CurValue.ToString();
                label1.Top = 2; this.Height = label1.Height + 4;
                panel1.Left = 75; panel1.Top = 0; //hardcode panel to right side
                panel1.Height = this.Height; //also hardcode height


                double PxlMax = this.Width;
                double _PxlValue = ((PxlMax - panel1.Left) / (MaxValue - MinValue)) * CurValue;
                int PxlValue = int.Parse(_PxlValue.ToString().Split('.')[0]);

                if (PxlValue <= this.Width - panel1.Left) panel1.Width = PxlValue;

                if (PxlValue + 10 + label1.Width < this.Width - panel1.Left)
                {
                    label1.Left = panel1.Left + PxlValue + 10;

                    label1.ForeColor = panel1.BackColor;
                    label1.BackColor = this.BackColor;
                }
                else
                {
                    label1.Left = panel1.Left + 2;
                    label1.ForeColor = Color.Black;
                    label1.BackColor = panel1.BackColor;
                }



            }

        }

    } //private void timer1_Tick(object sender, EventArgs e)





    //we have an XML file which contains templates for the graph module...
    //basically various session relevance statements which provide an easy
    //way for end users to choose default graphs.  We modify the XML and 
    //additional graphs are available... without recompiling!
    //this class is a baseline of info contained within that xml...
    //this is used to ship around the template data without 
    //extantiating a full modGraph.
    public class BigFixBarTemplates
    {
        public string Title;
        public string MinRelevance;
        public string Relevance;
        public string MaxRelevance;
        public int Frequency = 5;
    }
}
