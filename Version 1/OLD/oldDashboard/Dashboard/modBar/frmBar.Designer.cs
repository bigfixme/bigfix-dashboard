﻿namespace Dashboard.BarModule
{
    partial class frmBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBar));
            this.textPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textUsername = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textPort = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textServer = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textTitle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textFrequency = new System.Windows.Forms.TextBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.textMinRelevance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textRelevance = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textMaxRelevance = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboTemplates = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TestLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(168, 324);
            this.textPassword.Name = "textPassword";
            this.textPassword.PasswordChar = '*';
            this.textPassword.Size = new System.Drawing.Size(218, 20);
            this.textPassword.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(104, 327);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Password:";
            // 
            // textUsername
            // 
            this.textUsername.Location = new System.Drawing.Point(168, 298);
            this.textUsername.Name = "textUsername";
            this.textUsername.Size = new System.Drawing.Size(218, 20);
            this.textUsername.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(104, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Username:";
            // 
            // textPort
            // 
            this.textPort.Location = new System.Drawing.Point(392, 272);
            this.textPort.Name = "textPort";
            this.textPort.Size = new System.Drawing.Size(89, 20);
            this.textPort.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(389, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "WebReports Port:";
            // 
            // textServer
            // 
            this.textServer.Location = new System.Drawing.Point(17, 272);
            this.textServer.Name = "textServer";
            this.textServer.Size = new System.Drawing.Size(369, 20);
            this.textServer.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 257);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "WebReports Server:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(479, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "* Each of these Value properties can be a number or session relevance which retur" +
    "ns a single value.";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(406, 354);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 28;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(325, 354);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 29;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textTitle
            // 
            this.textTitle.Location = new System.Drawing.Point(50, 54);
            this.textTitle.Name = "textTitle";
            this.textTitle.Size = new System.Drawing.Size(258, 20);
            this.textTitle.TabIndex = 27;
            this.textTitle.TextChanged += new System.EventHandler(this.textTitle_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Title:";
            // 
            // textFrequency
            // 
            this.textFrequency.Location = new System.Drawing.Point(126, 92);
            this.textFrequency.Name = "textFrequency";
            this.textFrequency.Size = new System.Drawing.Size(52, 20);
            this.textFrequency.TabIndex = 25;
            this.textFrequency.Text = "5";
            this.textFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textFrequency.TextChanged += new System.EventHandler(this.textFrequency_TextChanged);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(184, 83);
            this.trackBar1.Maximum = 30;
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(297, 42);
            this.trackBar1.TabIndex = 24;
            this.trackBar1.Value = 5;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Frequency (Minutes):";
            // 
            // textMinRelevance
            // 
            this.textMinRelevance.Location = new System.Drawing.Point(101, 131);
            this.textMinRelevance.Name = "textMinRelevance";
            this.textMinRelevance.Size = new System.Drawing.Size(380, 20);
            this.textMinRelevance.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Minimum Value:";
            // 
            // textRelevance
            // 
            this.textRelevance.Location = new System.Drawing.Point(101, 157);
            this.textRelevance.Name = "textRelevance";
            this.textRelevance.Size = new System.Drawing.Size(380, 20);
            this.textRelevance.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "Current Value:";
            // 
            // textMaxRelevance
            // 
            this.textMaxRelevance.Location = new System.Drawing.Point(101, 183);
            this.textMaxRelevance.Name = "textMaxRelevance";
            this.textMaxRelevance.Size = new System.Drawing.Size(380, 20);
            this.textMaxRelevance.TabIndex = 44;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "Maximum Value:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Location = new System.Drawing.Point(17, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(464, 2);
            this.panel1.TabIndex = 47;
            // 
            // comboTemplates
            // 
            this.comboTemplates.FormattingEnabled = true;
            this.comboTemplates.Location = new System.Drawing.Point(79, 12);
            this.comboTemplates.Name = "comboTemplates";
            this.comboTemplates.Size = new System.Drawing.Size(402, 21);
            this.comboTemplates.TabIndex = 46;
            this.comboTemplates.SelectedIndexChanged += new System.EventHandler(this.comboTemplates_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Templates:";
            // 
            // TestLabel
            // 
            this.TestLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TestLabel.AutoSize = true;
            this.TestLabel.BackColor = System.Drawing.Color.DimGray;
            this.TestLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestLabel.ForeColor = System.Drawing.Color.Lime;
            this.TestLabel.Location = new System.Drawing.Point(10, 356);
            this.TestLabel.Name = "TestLabel";
            this.TestLabel.Size = new System.Drawing.Size(73, 24);
            this.TestLabel.TabIndex = 48;
            this.TestLabel.Text = "14 Day";
            this.TestLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TestLabel.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(314, 57);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(167, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "* Limited by space to 75pxl wide...";
            // 
            // frmBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 389);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TestLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.comboTemplates);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textMaxRelevance);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textRelevance);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textMinRelevance);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textUsername);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textPort);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textServer);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textFrequency);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bar - Properties";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textPort;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textServer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textFrequency;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textMinRelevance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textRelevance;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textMaxRelevance;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboTemplates;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label TestLabel;
        private System.Windows.Forms.Label label13;
    }
}