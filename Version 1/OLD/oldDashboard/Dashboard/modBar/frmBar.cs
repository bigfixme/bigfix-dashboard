﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Dashboard.BarModule
{
    public partial class frmBar : Form
    {
        frmMain fm;
        public frmBar(frmMain fmain)
        {
            fm = fmain;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        modBar MyBar = null;
        public void PreloadValues(modBar mod)
        {
            MyBar = mod;

            textTitle.Text = mod.Title;
            textFrequency.Text = mod.Frequency.ToString();
            trackBar1.Value = mod.Frequency;
            textMinRelevance.Text = mod.MinRelevance;
            textRelevance.Text = mod.Relevance;
            textMaxRelevance.Text = mod.MaxRelevance;

            textServer.Text = mod.ServerName;
            textPort.Text = mod.PortNumber;
            textUsername.Text = mod.Username;
            textPassword.Text = mod.Password;
        }


        List<BigFixBarTemplates> _Templates;
        public void LoadTemplates(List<BigFixBarTemplates> Templates)
        {
            _Templates = Templates;

            foreach (BigFixBarTemplates gt in Templates)
            {
                comboTemplates.Items.Add(gt.Title);

                if (gt.Frequency >= trackBar1.Maximum) trackBar1.Maximum = gt.Frequency;
            }

        }

        private void comboTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (BigFixBarTemplates gt in _Templates)
            {
                if (comboTemplates.Text == gt.Title)
                {
                    textTitle.Text = gt.Title;
                    textFrequency.Text = gt.Frequency.ToString();
                    trackBar1.Value = gt.Frequency;
                    textMinRelevance.Text = gt.MinRelevance;
                    textRelevance.Text = gt.Relevance;
                    textMaxRelevance.Text = gt.MaxRelevance;

                    break;
                }
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            textFrequency.Text = trackBar1.Value.ToString();
        }

        private void textFrequency_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(textFrequency.Text, out val);

            trackBar1.Value = val;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MyBar != null)
            {
                MyBar.Title = textTitle.Text;
                MyBar.Frequency = trackBar1.Value;
                MyBar.MinRelevance = textMinRelevance.Text;
                MyBar.Relevance = textRelevance.Text;
                MyBar.MaxRelevance = textMaxRelevance.Text;

                MyBar.ServerName = textServer.Text;
                MyBar.PortNumber = textPort.Text;
                MyBar.Username = textUsername.Text;
                MyBar.Password = textPassword.Text;
            }
            else
            {
                fm.NewBar(textTitle.Text, trackBar1.Value, textMinRelevance.Text, textRelevance.Text, textMaxRelevance.Text, textServer.Text, textPort.Text, textUsername.Text, textPassword.Text);
            }

            this.Close();
        }

        private void textTitle_TextChanged(object sender, EventArgs e)
        {
            TestLabel.Text = textTitle.Text;
            if (TestLabel.Width > 73)
            {
                textTitle.Text = textTitle.Text.Substring(0, textTitle.Text.Length - 1);
                textTitle.SelectionStart = textTitle.Text.Length;
            }
        }




    }
}
