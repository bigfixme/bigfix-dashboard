﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ZedGraph;
using System.Xml;

namespace Dashboard
{
    public partial class frmMain : Form
    {
        public List<GraphModule.BigFixGraphTemplates> Templates = new List<GraphModule.BigFixGraphTemplates>();
        
        public frmMain()
        {
            InitializeComponent();
        }


        private void frmMain_Load(object sender, EventArgs e)
        {
            this.Top = Screen.AllScreens[Properties.Settings.Default.Monitor].WorkingArea.Y;
            this.Left = Screen.AllScreens[Properties.Settings.Default.Monitor].WorkingArea.X;
            this.Width = Screen.AllScreens[Properties.Settings.Default.Monitor].WorkingArea.Width;
            this.Height = Screen.AllScreens[Properties.Settings.Default.Monitor].WorkingArea.Height;


            //fill context menu with number of monitors...

            for (int i = 0; i < Screen.AllScreens.Length; i++)
            {
                //MessageBox.Show(Screen.AllScreens[i].WorkingArea.ToString());

                ToolStripMenuItem tsi = new ToolStripMenuItem();
                tsi.Text = "Monitor " + (i + 1).ToString();

                tsi.Click += new EventHandler(tsi_Click);


                monitorToolStripMenuItem.DropDownItems.Add(tsi);
            }

            LoadGraphTemplates(); //pull in graph templates from the xml configuration file

            //load last profile...
            if (File.Exists(Properties.Settings.Default.LastLoadedProfile))
            {
                LoadProfile(Properties.Settings.Default.LastLoadedProfile);
            }
        }

        void frmMain_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (ProfileChanged)
            {
                DialogResult res = MessageBox.Show("Save current profile?", "Save Profile?", MessageBoxButtons.YesNoCancel);
                switch (res)
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        //save profile
                        if (Properties.Settings.Default.LastLoadedProfile == "")
                        {
                            saveFileDialog1.Title = "Save Profile";

                            saveFileDialog1.Filter = "Profile Files(*.XML)|*.XML|All files (*.*)|*.*";
                            DialogResult result = saveFileDialog1.ShowDialog();
                            if (result == DialogResult.OK) // Test result.
                            {
                                string xml = ExportProfile();
                                if (File.Exists(saveFileDialog1.FileName)) File.Delete(saveFileDialog1.FileName);

                                using (StreamWriter outfile = new StreamWriter(saveFileDialog1.FileName))
                                {
                                    outfile.Write(xml);
                                }
                            }
                            else if (result == DialogResult.Cancel)
                            {
                                //stop app exit
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            string xml = ExportProfile();
                            if (File.Exists(Properties.Settings.Default.LastLoadedProfile)) File.Delete(Properties.Settings.Default.LastLoadedProfile);

                            using (StreamWriter outfile = new StreamWriter(Properties.Settings.Default.LastLoadedProfile))
                            {
                                outfile.Write(xml);
                            }
                        }

                        break;

                    case System.Windows.Forms.DialogResult.No:
                        //do nothing
                        break;

                    case System.Windows.Forms.DialogResult.Cancel:
                        //stop app exit
                        e.Cancel = true;
                        break;
                }
            }



            if (saveFileDialog1.FileName != "" && !(Properties.Settings.Default.LastLoadedProfile.ToLower().Contains(saveFileDialog1.FileName.ToLower())))
            {
                Properties.Settings.Default.LastLoadedProfile = saveFileDialog1.FileName;
                Properties.Settings.Default.Save();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        void frmMain_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (timer1.Enabled) timer1_Tick(sender, e);

            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(new Point(this.Left + e.X, this.Top + e.Y));
            }
        }



        #region "Identify Monitors"
        List<frmMonitorIdentify> mi = new List<frmMonitorIdentify>();
        private void identifyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Screen.AllScreens.Length; i++)
            {
                frmMonitorIdentify _mi = new frmMonitorIdentify();
                _mi.label = i + 1;
                _mi.Left = Screen.AllScreens[i].WorkingArea.Left + (Screen.AllScreens[i].WorkingArea.Width - _mi.Width) / 2;
                _mi.Top = Screen.AllScreens[i].WorkingArea.Top + (Screen.AllScreens[i].WorkingArea.Height - _mi.Height) / 2;
                _mi.Show();
                mi.Add(_mi);
            }

            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (mi.Count > 0)
            {
                for (int i = 0; i < mi.Count; i++)
                {
                    mi[i].Close();
                    mi.Remove(mi[i]);
                    i--;
                }
            }
            timer1.Enabled = false;
        }


        void tsi_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(((ToolStripMenuItem)sender).Text);

            int i = int.Parse(((ToolStripMenuItem)sender).Text.Split(' ')[1]) - 1;

            //MessageBox.Show(Screen.AllScreens[i].WorkingArea.ToString());



            this.Top = Screen.AllScreens[i].WorkingArea.Y;
            this.Left = Screen.AllScreens[i].WorkingArea.X;
            this.Width = Screen.AllScreens[i].WorkingArea.Width;
            this.Height = Screen.AllScreens[i].WorkingArea.Height;


            Properties.Settings.Default.Monitor = i;
            Properties.Settings.Default.Save();
        }
        #endregion


        #region "Background Image"
        Image DefaultPicture;

        private void defaultPictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Load Background Image";
            openFileDialog1.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*";
            openFileDialog1.FileName = BackgroundFilename;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                LoadDefaultPicture(openFileDialog1.FileName);
            }
            else
            {
                BackgroundFilename = "";
                DefaultPicture = null;
                this.BackgroundImage = null;
            }

            ProfileChanged = true;
        }

        string BackgroundFilename = "";
        public void LoadDefaultPicture(string filename)
        {
            BackgroundFilename = filename;

            //Bitmap bm = (Bitmap)Image.FromFile(openFileDialog1.FileName);
            DefaultPicture = (Bitmap)Image.FromFile(filename);
            this.BackgroundImage = DefaultPicture;

            ProfileChanged = true;

        }

        #endregion





        


        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProperties p = new frmProperties();
            p.ShowDialog();
        }







        //code activated - here we take a profile or code from the registry and load all the related modules
        public void LoadGraphTemplates()
        {
            string filename = "./modGraph/GraphTemplates.xml";

            //if (!File.Exists(filename)) return;

            //string xml = File.ReadAllText(LastProfilePath);
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);

            XmlTextReader textReader = new XmlTextReader(filename);
            textReader.Read();


            bool processTemplates = false;
            bool processGraph = false;
            string attrType = "";
            

            //various module properties
            GraphModule.BigFixGraphTemplates TempGraph = null;

            while (textReader.Read())
            {
                XmlNodeType nType = textReader.NodeType;

                //start PROFILE
                if (nType == XmlNodeType.Element && textReader.Name.ToLower() == "templates") processTemplates = true;

                if (processTemplates)
                {
                    //=========================
                    //start OPTIONS
                    if (nType == XmlNodeType.Element && textReader.Name.ToLower() == "graph")
                    {
                        TempGraph = new GraphModule.BigFixGraphTemplates();
                        processGraph = true;
                    }

                    if (processGraph)
                    {
                        if (nType == XmlNodeType.Element) attrType = textReader.Name.ToLower();
                        if (nType == XmlNodeType.Text)
                        {
                            switch (attrType)
                            {
                                case "title":
                                    TempGraph.Title = textReader.Value;
                                    break;
                                case "frequency":
                                    TempGraph.Frequency = int.Parse(textReader.Value);
                                    break;
                                case "relevance":
                                    TempGraph.Relevance = textReader.Value;
                                    break;
                            }
                        }

                        if (nType == XmlNodeType.EndElement) attrType = "";
                    } //if (processModule)

                    //stop OPTIONS
                    if (nType == XmlNodeType.EndElement && textReader.Name.ToLower() == "graph")
                    {
                        Templates.Add(TempGraph);
                        processGraph = false;
                    }
                    //=========================
                    
                } //if (processProfile)

                //stop PROFILE
                if (nType == XmlNodeType.EndElement && textReader.Name == "templates")
                {
                    processTemplates = false;
                    break;  //only one profile object is allowed per file, so we can exit the reader at this point.
                }

            } //while (textReader.Read())


            textReader.Close();
        }

        




        #region "Profiles"
        public bool ProfileChanged = false;
        
        
        public void LoadProfile(string filename)
        {
            XmlTextReader textReader = new XmlTextReader(filename);
            textReader.Read();
            
            bool processProfile = false;
            bool processProfileProp = false;
            bool process = false;
            string attrType = "";
            Module mod = null;


            try
            {

                while (textReader.Read())
                {
                    XmlNodeType nType = textReader.NodeType;

                    if (nType == XmlNodeType.Element && textReader.Name.ToLower() == "profile") processProfile = true;
                    if (processProfile)
                    {
                        if (nType == XmlNodeType.Element && textReader.Name.ToLower() == "background") { processProfileProp = true; }
                        if (processProfileProp && nType == XmlNodeType.Text) LoadDefaultPicture(textReader.Value);
                        if (nType == XmlNodeType.EndElement && textReader.Name.ToLower() == "background") { processProfileProp = false; }


                        //=========================
                        if (nType == XmlNodeType.Element && textReader.Name.ToLower() == "graph") { mod = new GraphModule.modGraph(); process = true; }
                        if (nType == XmlNodeType.Element && textReader.Name.ToLower() == "service") { mod = new ServiceModule.modService(); process = true; }
                        if (nType == XmlNodeType.Element && textReader.Name.ToLower() == "bar") { mod = new BarModule.modBar(); process = true; }
                        if (process)
                        {
                            if (nType == XmlNodeType.Element) attrType = textReader.Name.ToLower();
                            if (nType == XmlNodeType.Text) mod.ConfigureProperty(attrType, textReader.Value);
                            if (nType == XmlNodeType.EndElement) attrType = "";
                        }
                        if (nType == XmlNodeType.EndElement && textReader.Name.ToLower() == "graph") { NewGraph(((GraphModule.modGraph)mod)); process = false; }
                        if (nType == XmlNodeType.EndElement && textReader.Name.ToLower() == "service") { NewService(((ServiceModule.modService)mod)); process = false; }
                        if (nType == XmlNodeType.EndElement && textReader.Name.ToLower() == "bar") { NewBar(((BarModule.modBar)mod)); process = false; }
                        //=========================

                    } //if (processProfile)
                    if (nType == XmlNodeType.EndElement && textReader.Name == "profile") { processProfile = false; break; }

                } //while (textReader.Read())

            }
            catch 
            {

            }
            finally
            {
                textReader.Close();
            }

            Properties.Settings.Default.LastLoadedProfile = filename;
            Properties.Settings.Default.Save();

            string[] file = filename.Split('\\');

            label2.Text = file[file.Length - 1].Replace(".XML", "").Replace(".XMl", "").Replace(".XmL", "").Replace(".xML", "").Replace(".xMl", "").Replace(".xmL", "").Replace(".xml", "");
        }


        public string EscapeString(string str)
        {
            str = str.Replace("&", "&amp;");
            str = str.Replace("\"", "&quot;");
            str = str.Replace("'", "&apos;");
            str = str.Replace("<", "&lt;");
            str = str.Replace(">", "&gt;");
            return str;
        }

        public string ExportProfile()
        {
            string xml = "";

            xml += "<?xml version=" + (char)34 + "1.0" + (char)34 + " encoding=" + (char)34 + "ISO-8859-1" + (char)34 + "?>" + Environment.NewLine;
            xml += "<!-- " + Application.ProductName + " v" + Application.ProductVersion + " Profile by dmoran@us.ibm.com at " + Application.CompanyName + "® -->" + Environment.NewLine + Environment.NewLine;

            xml += "<profile>" + Environment.NewLine;

            xml += " <background>" + EscapeString(BackgroundFilename) + "</background>" + Environment.NewLine;

            //export options
            xml += " <modules>" + Environment.NewLine + Environment.NewLine;

            foreach (object crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Module")) xml += ((Module)crtl).ExportModule();
            }

            xml += Environment.NewLine + " </modules>";
            xml += "</profile>";

            return xml;
        }


        #endregion









        #region "Bar Module"
        private void newBarCounterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BarModule.frmBar g = new BarModule.frmBar(this);
            //g.LoadTemplates(Templates);
            g.ShowDialog();
        }

        public void NewBar(BarModule.modBar mod)
        {
            mod.OnCloseModule += new BarModule.modBar.NewCloseModuleDelegate(this.Module_Close);
            mod.OnMoveModule += new BarModule.modBar.NewMoveModuleDelegate(this.Module_Move);
            this.Controls.Add(mod);
            this.Controls.SetChildIndex(mod, 0);
            mod.Visible = true;
        }

        public void NewBar(string Title, int Frequency, string MinRelevance, string Relevance, string MaxRelevance, string ServerName, string PortNumber, string Username, string Password, int top = 45, int left = 0, int width = 300, int height = 28)
        {
            BarModule.modBar mod = new BarModule.modBar();
            mod.OnCloseModule += new BarModule.modBar.NewCloseModuleDelegate(this.Module_Close);
            mod.OnMoveModule += new BarModule.modBar.NewMoveModuleDelegate(this.Module_Move);

            mod.Title = Title;
            mod.Frequency = Frequency;
            mod.MinRelevance = MinRelevance;
            mod.Relevance = Relevance;
            mod.MaxRelevance = MaxRelevance;
            mod.ServerName = ServerName;
            mod.PortNumber = PortNumber;
            mod.Username = Username;
            mod.Password = Password;
            mod.Top = top;
            mod.Left = left;
            mod.Width = width;
            mod.Height = height;

            this.Controls.Add(mod);
            this.Controls.SetChildIndex(mod, 0);
            mod.Visible = true;
        }

        #endregion


        #region "Graph Module"

        private void newModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GraphModule.frmGraph g = new GraphModule.frmGraph(this);
            g.LoadTemplates(Templates);
            g.ShowDialog();
        }

        public void NewGraph(GraphModule.modGraph mod)
        {
            mod.OnCloseModule += new GraphModule.modGraph.NewCloseModuleDelegate(this.Module_Close);
            mod.OnMoveModule += new GraphModule.modGraph.NewMoveModuleDelegate(this.Module_Move);
            this.Controls.Add(mod);
            this.Controls.SetChildIndex(mod, 0);
            mod.Visible = true;
        }

        public void NewGraph(string Title, int Frequency, string Relevance, string ServerName, string PortNumber, string Username, string Password, int top = 45, int left = 0, int width = 300, int height = 200)
        {
            GraphModule.modGraph mod = new GraphModule.modGraph();
            mod.OnCloseModule += new GraphModule.modGraph.NewCloseModuleDelegate(this.Module_Close);
            mod.OnMoveModule += new GraphModule.modGraph.NewMoveModuleDelegate(this.Module_Move);

            mod.Title = Title;
            mod.Frequency = Frequency;
            mod.Relevance = Relevance;
            mod.ServerName = ServerName;
            mod.PortNumber = PortNumber;
            mod.Username = Username;
            mod.Password = Password;
            mod.Top = top;
            mod.Left = left;
            mod.Width = width;
            mod.Height = height;

            this.Controls.Add(mod);
            this.Controls.SetChildIndex(mod, 0);
            mod.Visible = true;
        }

        #endregion






        #region "Service Module"
        
        private void newServiceMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServiceModule.frmService s = new ServiceModule.frmService(this);
            s.ShowDialog();
        }

        public void NewService(ServiceModule.modService mod)
        {
            mod.OnCloseModule += new ServiceModule.modService.NewCloseModuleDelegate(this.Module_Close);
            mod.OnMoveModule += new ServiceModule.modService.NewMoveModuleDelegate(this.Module_Move);
            
            this.Controls.Add(mod);
            this.Controls.SetChildIndex(mod, 0);
            mod.Visible = true;
        }

        public void NewService(string Title, int Frequency, string ServiceName, string ServerName, int top = 45, int left = 0, int width = 143, int height = 26)
        {
            ServiceModule.modService mod = new ServiceModule.modService();
            mod.OnCloseModule += new ServiceModule.modService.NewCloseModuleDelegate(this.Module_Close);
            mod.OnMoveModule += new ServiceModule.modService.NewMoveModuleDelegate(this.Module_Move);

            mod.Title = Title;
            mod.Frequency = Frequency;
            mod.ServiceName = ServiceName;
            mod.ServerName = ServerName;

            mod.Top = top;
            mod.Left = left;
            mod.Width = width;
            mod.Height = height;

            this.Controls.Add(mod);
            this.Controls.SetChildIndex(mod, 0);
            mod.Visible = true;
        }
        #endregion

        public void Module_Close(object sender, EventArgs e)
        {
            this.Controls.Remove((Module)sender);
        }

        public void Module_Move(object sender, EventArgs e)
        {
            ProfileChanged = true;
        }

        private void openProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clearProfileToolStripMenuItem1_Click(sender, e);

            openFileDialog1.Title = "Open Profile";
            openFileDialog1.Filter = "Profile Files(*.XML)|*.XML|All files (*.*)|*.*";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                LoadProfile(openFileDialog1.FileName);
            }
        }

        private void saveProfileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //save profile
            if (Properties.Settings.Default.LastLoadedProfile != "")
                saveFileDialog1.Title = "Save Profile As";
            else saveFileDialog1.Title = "Save Profile";

            saveFileDialog1.Filter = "Profile Files(*.XML)|*.XML|All files (*.*)|*.*";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                string xml = ExportProfile();

                if (File.Exists(saveFileDialog1.FileName)) File.Delete(saveFileDialog1.FileName);

                using (StreamWriter outfile = new StreamWriter(saveFileDialog1.FileName))
                {
                    outfile.Write(xml);
                }

                Properties.Settings.Default.LastLoadedProfile = saveFileDialog1.FileName;
                Properties.Settings.Default.Save();

                string[] file = Properties.Settings.Default.LastLoadedProfile.Split('\\');
                label2.Text = file[file.Length - 1].Replace(".XML", "").Replace(".XMl", "").Replace(".XmL", "").Replace(".xML", "").Replace(".xMl", "").Replace(".xmL", "").Replace(".xml", "");

                ProfileChanged = false;
            }
            else if (result == DialogResult.Cancel)
            {
                //stop app exit
            }
        }

        private void clearProfileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Module")) this.Controls.Remove(crtl);
            }

            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Module")) this.Controls.Remove(crtl);
            }

            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Module")) this.Controls.Remove(crtl);
            }

            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Module")) this.Controls.Remove(crtl);
            }

            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Module")) this.Controls.Remove(crtl);
            }

            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Module")) this.Controls.Remove(crtl);
            }

            label2.Text = "UnNamed Profile";
            saveFileDialog1.FileName = "";
            Properties.Settings.Default.LastLoadedProfile = "";
            Properties.Settings.Default.Save();
        }


        




    }




}
