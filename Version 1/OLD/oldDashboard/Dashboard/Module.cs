﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Dashboard
{
    public class Module : UserControl
    {
        //virtual = not required to be implemented at child level
        //abstract = required to implement by child
        public virtual string Title { get; set; }
        public virtual int Frequency { get; set; }
        

        
        //Closing Module event
        public delegate void NewCloseModuleDelegate(object sender, EventArgs e);
        public event NewCloseModuleDelegate OnCloseModule;
        protected void CloseModule(object sender, EventArgs e)
        {
            if (OnCloseModule != null) OnCloseModule(sender, e);
        }
        
        

        #region "Moving Module"
        bool isMouseLeftDown = false;
        int X, Y;
        
        public delegate void NewMoveModuleDelegate(object sender, EventArgs e);
        public event NewMoveModuleDelegate OnMoveModule;
        protected void MoveModule(object sender, EventArgs e)
        {
            if (OnMoveModule != null) OnMoveModule(sender, e);
        }

        public virtual void modMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            isMouseLeftDown = true;
            X = e.X;
            Y = e.Y;
        }

        public virtual void modMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            isMouseLeftDown = false;

            MoveModule(this, null);
        }

        public virtual void modMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (isMouseLeftDown)
                {
                    if (this.Left + (e.X - X) >= this.Parent.Width - this.Width - 10)
                    {
                        this.Left = this.Parent.Width - this.Width;
                    }
                    else if (this.Left + (e.X - X) <= 10)
                    {
                        this.Left = 0;
                    }
                    else
                    {
                        this.Left = this.Left + (e.X - X);
                    }

                    if (this.Top + (e.Y - Y) >= this.Parent.Height - this.Height - 10)
                    {
                        this.Top = this.Parent.Height - this.Height;
                    }
                    else if (this.Top + (e.Y - Y) <= 55)
                    {
                        this.Top = 45;
                    }
                    else
                    {
                        this.Top = this.Top + (e.Y - Y);
                    }
                }
        }
        #endregion



        public virtual string ExportModule()
        {
            string xml = "";

            xml += "    <title>" + ((frmMain)this.Parent).EscapeString(Title) + "</title>" + Environment.NewLine;
            xml += "    <frequency>" + Frequency + "</frequency>" + Environment.NewLine;

            xml += "    <top>" + this.Top.ToString() + "</top>" + Environment.NewLine;
            xml += "    <left>" + this.Left.ToString() + "</left>" + Environment.NewLine;
            xml += "    <width>" + this.Width.ToString() + "</width>" + Environment.NewLine;
            xml += "    <height>" + this.Height.ToString() + "</height>" + Environment.NewLine;

            return xml;
        }


        public virtual void ConfigureProperty(string PropertyName, string value)
        {
            switch (PropertyName)  //Module - generic properties
            {
                case "title": this.Title = value; break;
                case "frequency": this.Frequency = int.Parse(value); break;
                case "top": this.Top = int.Parse(value); break;
                case "left": this.Left = int.Parse(value); break;
                case "width": this.Width = int.Parse(value); break;
                case "height": this.Height = int.Parse(value); break;
            }
        }



    }
}
