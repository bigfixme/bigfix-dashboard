﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace Dashboard.GraphModule
{
    public partial class modGraph : Module
    {
        #region "Properties"
        public override string Title { get { return label1.Text; } set { label1.Text = value; } }
        public string Relevance = "";

        public string ServerName = "";
        public string PortNumber = "";
        public string Username = "";
        public string Password = "";
        

        public override string ExportModule()
        {
            string xml = "  <graph>" + Environment.NewLine;

            xml += base.ExportModule();

            xml += "    <relevance>" + ((frmMain)this.Parent).EscapeString(Relevance) + "</relevance>" + Environment.NewLine;

            xml += "    <servername>" + ((frmMain)this.Parent).EscapeString(ServerName) + "</servername>" + Environment.NewLine;
            xml += "    <portnumber>" + PortNumber + "</portnumber>" + Environment.NewLine;
            xml += "    <username>" + ((frmMain)this.Parent).EscapeString(Username) + "</username>" + Environment.NewLine;
            xml += "    <password>" + Password + "</password>" + Environment.NewLine;

            xml += "  </graph>" + Environment.NewLine + Environment.NewLine;
            return xml;
        }
        
        public override void ConfigureProperty(string PropertyName, string value)
        {
            switch (PropertyName)  //Module - generic properties
            {
                case "relevance": this.Relevance = value; break;

                case "servername": this.ServerName = value; break;
                case "portnumber": this.PortNumber = value; break;
                case "username": this.Username = value; break;
                case "password": this.Password = value; break;

                default:
                    base.ConfigureProperty(PropertyName, value);
                    break;
            }
        }


        PointPairList list = new PointPairList();

        #endregion

        public modGraph()
        {
            InitializeComponent();

            CreateGraph(zg1);
            SetSize();
            //zg1.GraphPane.Chart.Fill = new Fill(this.BackColor);

            zg1.GraphPane.Fill = new Fill(Color.DimGray);
            zg1.GraphPane.Legend.IsVisible = false;
        }
        
        
        void modEndpointCount_Resize(object sender, System.EventArgs e)
        {
            //SetSize();
        }



        bool Maximized = false;
        public int savedL;
        public int savedT;
        public int savedW;
        public int savedH;
        void modGraph_DoubleClick(object sender, System.EventArgs e)
        {
            if (Maximized)
            {
                this.Top = savedT; this.Left = savedL;
                this.Width = savedW; this.Height = savedH;
                Maximized = false;
            }
            else
            {
                savedT = this.Top; savedL = this.Left;
                savedW = this.Width; savedH = this.Height;

                this.Parent.Controls.SetChildIndex(this, 0);

                this.Top = 0; this.Left = 0;
                this.Width = this.Parent.Width; this.Height = this.Parent.Height;
                Maximized = true;
            }
            MoveModule(this, null);
        }




        #region "Moving Module"
        void modEndpointCount_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                base.modMouseDown(sender, e);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                contextMenuStrip1.Show(new Point(this.Parent.Left + this.Left + e.X, this.Parent.Top + this.Top + e.Y));
            }
        }

        void modEndpointCount_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseUp(sender, e);
        }

        void modEndpointCount_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseMove(sender, e);
        }
        #endregion




        
        //Remove Module
        private void removeModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseModule(this, null);
        }

        //Alternative Remove Module
        private void label2_Click(object sender, EventArgs e)
        {
            CloseModule(this, null);
        }

        //Launch settings dialog
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGraph g = new frmGraph( ((frmMain)this.Parent) );
            g.LoadTemplates(((frmMain)this.Parent).Templates);

            g.PreloadValues(this);

            g.ShowDialog();
        }





        public void LoadPoints(string label, PointPairList points)
        {
            // Generate a blue curve with circle symbols, and "My Curve 2" in the legend
            LineItem myCurve = zg1.GraphPane.AddCurve(label, points, Color.Blue, SymbolType.Circle);

            // Fill the area under the curve with a white-red gradient at 45 degrees
            myCurve.Line.Fill = new Fill(Color.White, Color.Red, 45F);

            // Make the symbols opaque by filling them with white
            myCurve.Symbol.Fill = new Fill(Color.White);

            // Calculate the Axis Scale Ranges
            zg1.GraphPane.AxisChange();
        }
                        
        private void CreateGraph(ZedGraphControl zgc)
        {
            GraphPane myPane = zgc.GraphPane;
                       

            // Set the titles and axis labels
            myPane.Title.IsVisible = false;
             myPane.XAxis.Title.Text = "Time";
            myPane.YAxis.Title.Text = "Count";

      

            // Fill the axis background with a color gradient
            myPane.Chart.Fill = new Fill(Color.White, Color.Black, 45F);

            // Fill the pane background with a color gradient
            myPane.Fill = new Fill(Color.White, Color.FromArgb(220, 220, 255), 45F);

        }
        
        private void SetSize()
        {
            zg1.Location = new Point(0, label1.Top + label1.Height + 2);
            // Leave a small margin around the outside of the control
            zg1.Size = new Size(this.ClientRectangle.Width, this.ClientRectangle.Height - zg1.Top);
        }










        int CurrentInterval = 0;
        vwebreports.RelevanceBinding bes = null;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (CurrentInterval <= 0)
            {
                if (ServerName != "" && Username != "" && Password != "")
                {
                    //1. retrieve latest value                    
                    if (bes == null)
                    {
                        bes = new vwebreports.RelevanceBinding(); ;
                        if (bes.Timeout != 10000) bes.Timeout = 10000;
                        if (bes.Url != "http://" + ServerName + ":" + PortNumber.ToString() + "/webreports?wsdl") bes.Url = "http://" + ServerName + ":" + PortNumber.ToString() + "/webreports?wsdl";
                    }


                    String[] res;
                    try
                    {
                        res = bes.GetRelevanceResult(Relevance, Username, Password);
                        PointPair pp = new PointPair(DateTime.Now.Minute, int.Parse(res[0]));

                        //2. add to list array
                        list.Add(pp);

                        //3. update graph
                        LoadPoints(DateTime.Now.TimeOfDay.ToString(), list);

                        //4. resize graph to fit display
                        zg1.RestoreScale(zg1.GraphPane);

                    }
                    catch 
                    {

                    }

                    timer1.Interval = 60000;
                }

                CurrentInterval = Frequency;
            }
            else
            {
                CurrentInterval--;
            }
        }

        


    }





    //we have an XML file which contains templates for the graph module...
    //basically various session relevance statements which provide an easy
    //way for end users to choose default graphs.  We modify the XML and 
    //additional graphs are available... without recompiling!
    //this class is a baseline of info contained within that xml...
    //this is used to ship around the template data without 
    //extantiating a full modGraph.
    public class BigFixGraphTemplates
    {
        public string Title;
        public string Relevance;
        public int Frequency = 5;
    }
}
