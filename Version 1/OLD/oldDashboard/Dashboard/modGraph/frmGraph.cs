﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace Dashboard.GraphModule
{
    public partial class frmGraph : Form
    {
        frmMain fm;
        public frmGraph(frmMain fmain)
        {
            fm = fmain;
            InitializeComponent();
        }

        
        modGraph MyGraph = null;
        public void PreloadValues(modGraph mod)
        {
            MyGraph = mod;

            textTitle.Text = mod.Title;
            textFrequency.Text = mod.Frequency.ToString();
            trackBar1.Value = mod.Frequency;
            textRelevance.Text = mod.Relevance;

            textServer.Text = mod.ServerName;
            textPort.Text = mod.PortNumber;
            textUsername.Text = mod.Username;
            textPassword.Text = mod.Password;
        }


        List<BigFixGraphTemplates> _Templates;
        public void LoadTemplates(List<BigFixGraphTemplates> Templates)
        {
            _Templates = Templates;

            foreach (BigFixGraphTemplates gt in Templates)
            {
                comboTemplates.Items.Add(gt.Title);

                if (gt.Frequency >= trackBar1.Maximum) trackBar1.Maximum = gt.Frequency;
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (BigFixGraphTemplates gt in _Templates)
            {
                if (comboTemplates.Text == gt.Title)
                {
                    textTitle.Text = gt.Title;
                    textFrequency.Text = gt.Frequency.ToString();
                    trackBar1.Value = gt.Frequency;
                    textRelevance.Text = gt.Relevance;

                    break;
                }
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            textFrequency.Text = trackBar1.Value.ToString();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            int val;
            int.TryParse(textFrequency.Text, out val);

            trackBar1.Value = val;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MyGraph != null)
            {
                MyGraph.Title = textTitle.Text;
                MyGraph.Frequency = trackBar1.Value;
                MyGraph.Relevance = textRelevance.Text;

                MyGraph.ServerName = textServer.Text;
                MyGraph.PortNumber = textPort.Text;
                MyGraph.Username = textUsername.Text;
                MyGraph.Password = textPassword.Text;
            }
            else
            {
                fm.NewGraph(textTitle.Text, trackBar1.Value, textRelevance.Text, textServer.Text, textPort.Text, textUsername.Text, textPassword.Text);
            }
            
            this.Close();
        }

        


    }
}
