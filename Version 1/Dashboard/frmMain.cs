﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System.IO;
using System.Xml.Linq;

namespace Dashboard
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            this.Top = Properties.Settings.Default.Top;
            this.Left = Properties.Settings.Default.Left;
            this.Width = Properties.Settings.Default.Width;
            this.Height = Properties.Settings.Default.Height;

            if (Properties.Settings.Default.TopMax)
            {
                this.WindowState = FormWindowState.Maximized;
                this.TopMost = true;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            }

            

            if (!string.IsNullOrEmpty(Properties.Settings.Default.CurrentModules))
            {

                try
                {
                    //now determine which modules need to be added to the view... based on the saved properties.
                    Debug.WriteLine("Load:  " + Properties.Settings.Default.CurrentModules);
                    string[] mods = Properties.Settings.Default.CurrentModules.Split(';');
                    foreach (string m in mods)
                    {
                        if (!string.IsNullOrEmpty(m.Trim()))
                        {
                            AddModule(m);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ERROR Opening:  " + ex.Message);
                }
            }

            //force a refresh so the modules get placed in their right locations
            Form1_Resize(this, null);


            //refresh buttons status...
            RefreshConfigButton();
            
            
            minRefresh = Properties.Settings.Default.RefreshTimer;
            if (curMin > minRefresh) curMin = minRefresh;
        }






        public Module AddModule(string moduleDefinition, string status = "")
        {
            string[] parts = moduleDefinition.Split('|');
            string[] grid = parts[0].Split('x');
            string[] size = parts[1].Split('x');
            string[] typetitle = parts[2].Split(':');

            Module m = null;

            switch (typetitle[0])
            {
                case "service":
                    modService svc = new modService();
                    svc.GRID_Column = int.Parse(grid[0]);
                    svc.GRID_Row = int.Parse(grid[1]);

                    svc.Width = int.Parse(size[0]);
                    svc.Height = int.Parse(size[1]);
                    svc.Title = typetitle[1].TrimEnd(';');
                    if (!string.IsNullOrEmpty(status)) svc.Status = status;
                    
                    svc.EditMode = EditMode;
                    //svc.guid = guid;
                    //Mod = svc;
                    Controls.Add(svc);
                    m = svc;
                    break;


                default:
                    modOther otr = new modOther();
                    otr.GRID_Column = int.Parse(grid[0]);
                    otr.GRID_Row = int.Parse(grid[1]);

                    otr.Width = int.Parse(size[0]);
                    otr.Height = int.Parse(size[1]);
                    otr.Title = typetitle[1].TrimEnd(';');
                    if (!string.IsNullOrEmpty(status)) otr.Status = status;

                    otr.EditMode = EditMode;
                    //otr.guid = guid;
                    //Mod = otr;
                    Controls.Add(otr);
                    m = otr;
                    break;
            }

            //adjust z order...
            try
            {
                Controls[Controls.Count - 1].BringToFront();
                return m;
            }
            catch { }
            return null;
        }














        void frmMain_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            try
            {
                string currentmodules = "";
                foreach (Control crtl in this.Controls)
                {
                    if (crtl.ToString().StartsWith("Dashboard.modOther"))
                    {
                        modOther mo = (modOther)crtl;

                        Debug.WriteLine("Saving:  " + mo.GRID_Column.ToString() + "x" + mo.GRID_Row.ToString() + "|" + mo.Width.ToString() + "x" + mo.Height.ToString() + "|other:" + mo.Title.Trim() + ";");
                        currentmodules += mo.GRID_Column.ToString() + "x" + mo.GRID_Row.ToString() + "|" + mo.Width.ToString() + "x" + mo.Height.ToString() + "|other:" + mo.Title.Trim() + ";";
                    }
                    else if (crtl.ToString().StartsWith("Dashboard.modService"))
                    {
                        modService mo = (modService)crtl;

                        Debug.WriteLine("Saving:  " + mo.GRID_Column.ToString() + "x" + mo.GRID_Row.ToString() + "|" + mo.Width.ToString() + "x" + mo.Height.ToString() + "|service:" + mo.Title.Trim() + ";");
                        currentmodules += mo.GRID_Column.ToString() + "x" + mo.GRID_Row.ToString() + "|" + mo.Width.ToString() + "x" + mo.Height.ToString() + "|service:" + mo.Title.Trim() + ";";
                    }

                }

                Debug.WriteLine("Saved:  " + currentmodules);

                Properties.Settings.Default.Top = this.Top;
                Properties.Settings.Default.Left = this.Left;
                    
                if (!Properties.Settings.Default.TopMax)
                {
                    Properties.Settings.Default.Width = this.Width;
                    Properties.Settings.Default.Height = this.Height;
                }

                Properties.Settings.Default.CurrentModules = currentmodules;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex) { Debug.WriteLine("ERROR Closing:  " + ex.Message); }            
        }

        //solves my flickering problem when repainting the background
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED 
                return cp;
            }
        }  

        
        void Form1_Resize(object sender, System.EventArgs e)
        {
            ////this is the smallest this form shoudl go.
            //if (this.Width < 675 || this.Height < 475)
            //{
            //    this.Width = 675;
            //    this.Height = 475;
            //}


            Grid.Clear();  //clear grid so we have an accurate grid size
            //calcualte grid based on size of this form.... ie: cut of extras and center grid
            int numX = (this.Width - SystemInformation.Border3DSize.Width*2-10) / 50;
            int offsetX = ((this.Width - SystemInformation.Border3DSize.Width*2) - numX * 50) / 2-2;
            int numY = (this.Height - SystemInformation.CaptionHeight - SystemInformation.Border3DSize.Width - button1.Height-10) / 50;
            int offsetY = ((this.Height - SystemInformation.CaptionHeight - SystemInformation.Border3DSize.Width + button1.Height) - numY * 50) / 2-3;

            for (int y = 0; y < numY; y++)
            {
                for (int x = 0; x < numX; x++)
                {
                    Grid.Add(new Point (x * 50 + offsetX, y * 50 + offsetY));
                }
            }


            GridSize.Text = "Grid: " + numX.ToString() + "x" + numY.ToString();
            GridSize.Left = this.Width - GridSize.Width - 10;
            GridSize.Top = (button1.Height - GridSize.Height)/2;

            //alert user if there are any modules beyond the borders of this grid size
            timer1.Enabled = false; GridSize.ForeColor = Color.Lime;
            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Dashboard.mod"))
                {
                    if (crtl.Left + crtl.Width > this.Width - Grid[0].X || crtl.Top + crtl.Height > this.Height - Grid[0].Y)
                    {
                        timer1.Enabled = true; GridSize.ForeColor = Color.Red;
                        break;
                    }
                }
            }



            //the grid was resized, and we may have modules out there that need to me adjusted slightly due to offsets
            foreach (Control crtl in this.Controls)
            {
                if (crtl.ToString().Contains("Dashboard.mod")) //module, thus adjust appropriately...
                {
                    Module m = (Module)crtl;

                    m.Left = Grid[0].X + m.GRID_Column * 50;
                    m.Top = Grid[0].Y + m.GRID_Row * 50;
                }
            }




            this.Refresh();
        }


        public List<Point> Grid = new List<Point>();  //holds the grid we are drawing in

        void Form1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            if (!EditMode) return;

            if (Grid.Count <= 0) return; //don't do anything unless we have a properly setup grid

            System.Drawing.Graphics formGraphics = this.CreateGraphics();

            formGraphics.Clear(Color.Black);


            System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Lime);
            formGraphics.DrawLine(myPen, 0, button1.Height, this.Width, button1.Height);

            System.Reflection.Assembly thisExe;
            thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream file = thisExe.GetManifestResourceStream("Dashboard.dashboard.png");

            ////calcualte grid based on size of this form.... ie: cut of extras and center grid
            //foreach (Point p in Grid) 
            //{
            //    formGraphics.DrawImage(Image.FromStream(file), p.X, p.Y);
            //}

            //myPen.Dispose();
            formGraphics.Dispose();           
        }






        

        bool _editmode = false;
        public bool EditMode
        {
            get { return _editmode; }
            set { 
                _editmode = value;
                foreach (Control crtl in this.Controls)
                {
                    if (crtl.ToString().StartsWith("Dashboard.mod"))
                    {
                        ((Module)crtl).EditMode = _editmode;
                    }
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            EditMode = !EditMode;

           

            if (EditMode)
                button1.BackColor = Color.Lime;
            else button1.BackColor = Color.Black;

            GridSize.Visible = EditMode;
            btnAdd.Visible = EditMode;
            btnConfig.Visible = EditMode;
            comboBox1.Visible = EditMode;
            btnRefresh.Visible = EditMode;
            
            if (btnRefresh.Enabled == false)
            {
                if (EditMode == true)
                    getstarted.Visible = false;
                else getstarted.Visible = true;
            }
            else if (btnRefresh.Enabled == true)
            {
                getstarted.Visible = false;
            }

            if (EditMode)
            {
                addmodules.Visible = false;
            }
            else
            {
                bool foundmodules = false;
                foreach (Control crtl in this.Controls)
                {
                    if (crtl.ToString().Contains("Dashboard.mod"))
                    {
                        foundmodules = true;
                        break;
                    }
                }
                addmodules.Visible = !foundmodules;
            }

            this.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0) return;

            int i = comboBox1.SelectedIndex;
            foreach (stat s in stats)
            {
                if (!s.DELETED)
                {
                    if (s.comboIndex != -1 && s.comboIndex == comboBox1.SelectedIndex)
                    {
                        Module m = AddModule("0x0|150x100|" + s.Type + ":" + s.Title + ";", s.Status);

                        if (m != null)
                        {
                            m.Left = Grid[0].X;
                            m.Top = Grid[0].Y;
                        }

                        ////now remove combo item since it is used... no need to do a full update...
                        //updateComboBox();

                        break;
                    }
                }
            }
        }

       

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (GridSize.ForeColor == Color.Red)
                GridSize.ForeColor = Color.Black;
            else GridSize.ForeColor = Color.Red;
        }

        private void btnServerURL_Click(object sender, EventArgs e)
        {
            frmConfig cfg = new frmConfig();

            cfg.TopMost = Properties.Settings.Default.TopMax;

            cfg.Owner = this;
            cfg.StartPosition = FormStartPosition.CenterParent;
            cfg.ShowDialog();

            if (minRefresh != Properties.Settings.Default.RefreshTimer)
            {
                minRefresh = Properties.Settings.Default.RefreshTimer;
                if (curMin > minRefresh) curMin = minRefresh;
            }


            if (Properties.Settings.Default.TopMax)
            {
                this.WindowState = FormWindowState.Maximized;
                this.TopMost = true;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.TopMost = false;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            }



            RefreshConfigButton();

            //RefreshStatus();
        }



        private void RefreshConfigButton()
        {
            System.Reflection.Assembly thisExe;
            thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream ok = thisExe.GetManifestResourceStream("Dashboard.Resources.ok.png");
            System.IO.Stream err = thisExe.GetManifestResourceStream("Dashboard.Resources.error.png");

            if (Properties.Settings.Default.ServerURL != "")
            {
                try
                {
                    Uri uri = new Uri(Properties.Settings.Default.ServerURL);
                    btnConfig.Image = Image.FromStream(ok);
                    btnRefresh.Enabled = true;
                    //comboBox1.Enabled = true;
                    //btnAdd.Enabled = true;

                    getstarted.Visible = false;

                    if (!EditMode)
                    {
                        bool foundmodules = false;
                        foreach (Control crtl in this.Controls)
                        {
                            if (crtl.ToString().Contains("Dashboard.mod"))
                            {
                                foundmodules = true;
                                break;
                            }
                        }
                        if (foundmodules)
                        {
                            getstarted.Visible = false;
                            addmodules.Visible = false;
                        }
                        else { addmodules.Visible = !getstarted.Visible; }
                    }


                    //RefreshStatus();
                }
                catch
                {
                    //comboBox1.Enabled = false;
                    //btnAdd.Enabled = false;
                    btnConfig.Image = Image.FromStream(err);
                    btnRefresh.Enabled = false;
                }
            }
            else
            {
                comboBox1.Enabled = false;
                btnAdd.Enabled = false;
                btnConfig.Image = Image.FromStream(err);
                btnRefresh.Enabled = false;
            }
        }




        List<stat> stats = new List<stat>();
        private void RefreshStatus(bool force = false)
        {
            Debug.WriteLine("Refreshing Status");
                        
            curMin = 60;  //set arbitrarily high to give us time to get a refresh count

            if (btnRefresh.Enabled || force)
            {
                //1. call SOAP and get back status.
                string xml = CallWebService();


                if (!string.IsNullOrEmpty(xml))
                {
                    //return;
                
                    //Console.WriteLine(xml);
                    //Console.WriteLine("");

                    //2. if all goes well then set ret to true.
                    XDocument doc = new XDocument();
                    XNamespace ns = "http://schemas.bigfix.me/status/";
                    try
                    {
                        doc = XDocument.Parse(xml);
                    } 
                    catch (Exception ex) 
                    {
                        Debug.WriteLine("ERROR Parsing XML:  " + ex.Message);
                    }


                    foreach (stat s in stats) { s.updated = false; }  //reset updated flag so we can remove items that were deleted server-side
                
                    try 
                    {
                        foreach (XElement element in doc.Elements().Elements().Elements().Elements())
                        {
                            bool foundit = false;

                            Debug.WriteLine(element.Attribute("category").Value + ": " + element.Attribute("title").Value);

                            foreach (stat s in stats)
                            {
                                if (!s.DELETED)
                                {
                                    Debug.WriteLine("----" + s.Category + ": " + s.Title);

                                    if (s.Category == element.Attribute("category").Value)
                                    {
                                        if (s.Title == element.Attribute("title").Value)
                                        {
                                            Debug.WriteLine("--------Found it!");
                                            //update values...
                                            try
                                            {
                                                s.Type = element.Attribute("type").Value;
                                                s.Status = element.Attribute("status").Value;
                                                s.updated = true;
                                            }
                                            catch (Exception ex) { Debug.WriteLine("ERROR Updating Properties:  " + ex.Message); }

                                            if (element.HasElements)
                                            {
                                                try
                                                {
                                                    foreach (XElement e in element.Elements())
                                                    {
                                                        foreach (properties prop in s.properties) { if (!prop.DELETED) prop.updated = false; } //reset updated flag so we can remove items that were deleted server-side

                                                        bool foundprop = false;
                                                        properties p = null;
                                                        foreach (properties prop in s.properties)
                                                        {
                                                            if (!prop.DELETED)
                                                            {
                                                                if (prop.name == e.Name.LocalName)
                                                                { //update existing property
                                                                    p = prop;
                                                                    foundprop = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        if (!foundprop)
                                                        { //add new property
                                                            p = new properties();
                                                            p.name = e.Name.LocalName;
                                                            p.value = e.Value;
                                                            p.updated = true;

                                                            s.properties.Add(p);
                                                        }
                                                        else { p.value = e.Value; p.updated = true; }

                                                        foreach (properties prop in s.properties) { if (!prop.DELETED && !prop.updated) prop.DELETED = true; } //delete removed properties

                                                    }
                                                }
                                                catch (Exception ex) { Debug.WriteLine("ERROR looping through elements:  " + ex.Message); }
                                            }

                                            foundit = true;
                                            break;
                                        }
                                    }
                                } //if (!s.DELETED) { 
                            }


                            if (!foundit)
                            { //add new stat
                                stat c = new stat();
                                c.Title = element.Attribute("title").Value;
                                c.Type = element.Attribute("type").Value;
                                c.Status = element.Attribute("status").Value;
                                c.Category = element.Attribute("category").Value;
                                c.updated = true;

                                if (element.HasElements)
                                {
                                    foreach (XElement e in element.Elements())
                                    {
                                        properties p = new properties();
                                        p.name = e.Name.LocalName;
                                        p.value = e.Value;

                                        c.properties.Add(p);
                                    }
                                }
                                stats.Add(c);
                            }
                        }
                                        
                    }
                    catch (Exception ex) { Debug.WriteLine("ERROR:  " + ex.Message); }




                    foreach (stat s in stats) { if (!s.updated) s.DELETED = true; } //delete removed stats




                    foreach (Control crtl in this.Controls)
                    {
                        if (crtl.ToString().StartsWith("Dashboard.mod"))
                        {
                            Module m = (Module)crtl;
                            m.Refreshed = false;
                        }
                    }


                    //now update each of the components
                    foreach (stat s in stats)
                    {
                        if (!s.DELETED)
                        {
                            foreach (Control crtl in this.Controls)
                            {
                                if (crtl.ToString().StartsWith("Dashboard.mod"))
                                {
                                    Module m = (Module)crtl;

                                    Debug.WriteLine(m.Title.Trim() + " == " + s.Title.Trim());
                                    if (m.Title.Trim() == s.Title.Trim())
                                    {
                                        m.Status = s.Status;
                                        m.Refreshed = true;
                                    }
                                }
                            }
                        }
                    }

                }
                else
                {
                    foreach (Control crtl in this.Controls)
                    {
                        if (crtl.ToString().StartsWith("Dashboard.mod"))
                        {
                            Module m = (Module)crtl;
                            m.Refreshed = false;
                        }
                    }
                }


                foreach (Control crtl in this.Controls)
                {
                    if (crtl.ToString().StartsWith("Dashboard.mod"))
                    {
                        Module m = (Module)crtl;

                        if (!m.Refreshed)
                        {
                            m.NoStatus = true;
                        }
                    }
                }




                //3. update/add each of the componets to a list and update the combobox as needed
                //   only add stats that are not already on the screen.
                updateComboBox();


                if (comboBox1.Items.Count > 0)
                {
                    comboBox1.Enabled = true;
                    btnAdd.Enabled = true;
                }
                else
                {
                    comboBox1.Enabled = false;
                    btnAdd.Enabled = false;
                }

            }

            curMin = minRefresh;
        } 
        








        int minRefresh = 5;
        int curMin = 5;
        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            curMin--; //countdown minutes...
            if (curMin <= 0) RefreshStatus();                
        }


        /// <summary>
        /// FUNCTION CALLED WHEN THE USER CLICKS THE REFRESH BUTTON
        /// BASICALLY WE'RE GOING TO CALL THE REFRESH COMMAND
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshStatus();
        }



        /// <summary>
        /// UPDATE THE COMBO BOX WHICH IS A LIST OF USER COMPONENTS THEY CAN ADD TO THEIR DASHBOARD
        /// PULLED DIRECTLY FROM THE BACKEND SERVICES XML FILE
        /// </summary>
        private void updateComboBox()
        {
            comboBox1.Items.Clear();
            comboBox1.Text = "";

            foreach (stat s in stats) { s.comboIndex = -1; }
            foreach (stat s in stats)
            {
                int i = comboBox1.Items.Add(s.Type + ": " + s.Title);
                s.comboIndex = i;                    
            }

            if (comboBox1.Items.Count > 0)
            {
                comboBox1.Enabled = true;
                btnAdd.Enabled = true;
            }
            else
            {
                comboBox1.Enabled = false;
                btnAdd.Enabled = false;
            }

        }






        /// <summary>
        /// COLLECTION OF CALLS RELATED TO PULLING THE STATUS FROM OUR BACKEND SERVICE
        /// </summary>
        #region "WebServiceCalls"

        public static string CallWebService()
        {
            try
            {
                var _url = Properties.Settings.Default.ServerURL + "?wsdl";
                var _action = "GetRelevanceResult";

                XmlDocument soapEnvelopeXml = CreateSoapEnvelope();
                HttpWebRequest webRequest = CreateWebRequest(_url, _action);
                InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

                // begin async call to web request. 
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete. You might want to 
                // do something usefull here like update your UI. 
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request. 
                string soapResult;
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }

                return soapResult;
            }
            catch { }
            return null;
        }
        
        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static XmlDocument CreateSoapEnvelope()
        {
            XmlDocument soapEnvelop = new XmlDocument();

            string xml = @"<?xml version=""1.0"" encoding=""utf-8"" ?>";
            xml += @"<env:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">";
            xml += @"<env:Body>";

            xml += @"<Command action=""status"" arguments="""" />";
            xml += @"<Credentials username=""" + Properties.Settings.Default.Username + @""" password=""" + Properties.Settings.Default.Password + @""" />";
            
            xml += @"</env:Body>";
            xml += @"</env:Envelope>";
            
            soapEnvelop.LoadXml(xml);
            return soapEnvelop;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            try
            {
                using (Stream stream = webRequest.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }
            }
            catch { }
        }

        #endregion


    }












    //OBJECT TO HOLD THE STATUS PULLED FROM THE BACKEND SERVICE
    class stat
    {
        public bool DELETED = false;

        string _title = "";
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        string _type = "";
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        string _status = "";
        public string Status
        {
            get { return _status; }
            set { 
                _status = value;
            }
        }

        string _category = "";
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        string _data = "";
        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }
              

        public List<properties> properties = new List<properties>();


        public bool updated = false;
        public int comboIndex = -1;
    }

    class properties
    {
        public string name = "";
        public string value = "";

        public bool DELETED = false;

        public bool updated = false;
    }




}
