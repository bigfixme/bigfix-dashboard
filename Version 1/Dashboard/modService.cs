﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dashboard
{
    public partial class modService : Module
    {
       
        public modService()
        {
            InitializeComponent();

            ShowEdit = false;
        }

        private void Module_Load(object sender, EventArgs e)
        {
            try { MAIN = (frmMain)this.Parent; }
            catch { }
        }

        
        #region "Moving Module"
        void Module_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                base.modMouseDown(sender, e);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                //contextMenuStrip1.Show(new Point(this.Parent.Left + this.Left + e.X, this.Parent.Top + this.Top + e.Y));
            }
        }

        void Module_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseUp(sender, e);
        }

        void Module_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseMove(sender, e);
        }
        #endregion



        


        protected override void OnPaint(PaintEventArgs e)
        {
                        
            System.Drawing.Graphics formGraphics = this.CreateGraphics();

            
            if (this.NoStatus)
            {
                formGraphics.DrawString(Title, new Font("Arial", 16, FontStyle.Regular), new SolidBrush(Color.Red), new Point(10, 13));
            }
            else
            {
                formGraphics.DrawString(Title, new Font("Arial", 16, FontStyle.Regular), new SolidBrush(Color.Silver), new Point(10, 13));
            }


            int cirD = this.Width / 5;
            int x = cirD / 2;

            int cirTop = (this.Height - 50 - cirD) / 2 + 50;

            //draw left circle
            if (Status == "Running")
                formGraphics.FillEllipse(new SolidBrush(Color.FromArgb(0, 192, 0)), x, cirTop, cirD, cirD);
            else formGraphics.FillEllipse(new SolidBrush(Color.FromArgb(34, 34, 34)), x, cirTop, cirD, cirD);
            formGraphics.DrawEllipse(new Pen(Color.FromArgb(17,17,17), 2f), x, cirTop, cirD, cirD);

            //draw middle circle
            if (Status == "Other")
                formGraphics.FillEllipse(new SolidBrush(Color.FromArgb(222, 222, 7)), x * 2 + cirD * 1, cirTop, cirD, cirD);
            else formGraphics.FillEllipse(new SolidBrush(Color.FromArgb(34, 34, 34)), x * 2 + cirD * 1, cirTop, cirD, cirD);
            formGraphics.DrawEllipse(new Pen(Color.FromArgb(17, 17, 17), 2f), x * 2 + cirD * 1, cirTop, cirD, cirD);

            //draw right circle
            if (Status == "Stopped")
                formGraphics.FillEllipse(new SolidBrush(Color.FromArgb(192, 0, 0)), x * 3 + cirD * 2, cirTop, cirD, cirD);
            else formGraphics.FillEllipse(new SolidBrush(Color.FromArgb(34, 34, 34)), x * 3 + cirD * 2, cirTop, cirD, cirD);
            formGraphics.DrawEllipse(new Pen(Color.FromArgb(17, 17, 17), 2f), x * 3 + cirD * 2, cirTop, cirD, cirD);

            //myPen.Dispose();
            formGraphics.Dispose();

            base.OnPaint(e);
            
        }


    }
}
