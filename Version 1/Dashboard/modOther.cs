﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dashboard
{
    public partial class modOther : Module
    {
        public modOther()
        {
            InitializeComponent();

            ShowEdit = false;
        }


        private void Module_Load(object sender, EventArgs e)
        {
            try { MAIN = (frmMain)this.Parent; }
            catch { }
        }


        #region "Moving Module"
        void Module_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                base.modMouseDown(sender, e);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                //contextMenuStrip1.Show(new Point(this.Parent.Left + this.Left + e.X, this.Parent.Top + this.Top + e.Y));
            }
        }

        void Module_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseUp(sender, e);
        }

        void Module_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.modMouseMove(sender, e);
        }
        #endregion




        protected override void OnPaint(PaintEventArgs e)
        {

            System.Drawing.Graphics formGraphics = this.CreateGraphics();

            
            

            if (this.NoStatus)
            {
                formGraphics.DrawString(Title, new Font("Arial", 16, FontStyle.Regular), new SolidBrush(Color.Red), new Point(10, 13));
            }
            else
            {
                formGraphics.DrawString(Title, new Font("Arial", 16, FontStyle.Regular), new SolidBrush(Color.Silver), new Point(10, 13));
            }

            formGraphics.DrawString(Status, new Font("Arial", 16, FontStyle.Regular), new SolidBrush(Color.White), new Point(10, 61));
            

            //myPen.Dispose();
            formGraphics.Dispose();

            base.OnPaint(e);

        }


    }
}
