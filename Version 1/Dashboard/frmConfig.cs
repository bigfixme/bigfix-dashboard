﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dashboard
{
    public partial class frmConfig : Form
    {
        public frmConfig()
        {
            InitializeComponent();

            this.checkBox1.Checked = Properties.Settings.Default.TopMax;
            this.textBox1.Text = Properties.Settings.Default.ServerURL;
            this.textBox2.Text = Properties.Settings.Default.Username;
            this.textBox3.Text = Properties.Settings.Default.Password;
            this.TimerRefresh.Value = Properties.Settings.Default.RefreshTimer;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.TopMax = this.checkBox1.Checked;
            Properties.Settings.Default.ServerURL = this.textBox1.Text;
            Properties.Settings.Default.Username = this.textBox2.Text;
            Properties.Settings.Default.Password = this.textBox3.Text;
            Properties.Settings.Default.RefreshTimer = (int)this.TimerRefresh.Value;

            Properties.Settings.Default.Save(); //save to disk so it loads up again next time.

            this.Close();
        }
    }
}
