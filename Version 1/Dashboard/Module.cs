﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace Dashboard
{
    public class Module : UserControl
    {

        //virtual = not required to be implemented at child level
        //abstract = required to implement by child
        public virtual int Frequency { get; set; }

        string _title = "Module";
        public virtual string Title { 
            get { return _title; }
            set { 
                _title = value;
            }
        }
        
        string _status = "";
        public string Status
        {
            get { return _status; }
            set {
                _status = value;
                _nostatus = false;
                this.Refresh();
            }
        }






        public Guid guid;

        public frmMain MAIN;

        //solves my flickering problem when repainting the background
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED 
                return cp;
            }
        }

        


        bool _editmode = false;
        public bool EditMode
        {
            get { return _editmode; }
            set { 
                _editmode = value;

                if (value)
                {
                    System.Reflection.Assembly thisExe;
                    thisExe = System.Reflection.Assembly.GetExecutingAssembly();
                    System.IO.Stream file = thisExe.GetManifestResourceStream("Dashboard.dashboard2.png");
                    this.BackgroundImageLayout = ImageLayout.Tile;
                    this.BackgroundImage = Image.FromStream(file);
                }
                else { this.BackgroundImage = null; }
            }
        }


        bool _showEdit = true;
        /// <summary>
        /// Show the Edit button when we are in EditMode
        /// </summary>
        public bool ShowEdit
        {
            get { return _showEdit; }
            set { _showEdit = value; }
        }

        bool _showDelete = true;
        /// <summary>
        /// Show the Delete button when we are in EditMode
        /// </summary>
        public bool ShowDelete
        {
            get { return _showDelete; }
            set { _showDelete = value; }
        }

        bool _showResize = true;
        /// <summary>
        /// Show the Resize marker when we are in EditMode
        /// </summary>
        public bool ShowResize
        {
            get { return _showResize; }
            set { _showResize = value; }
        }



        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.FromArgb(112,112,112));

            
            myPen.Width = 2;

            System.Drawing.Graphics formGraphics = this.CreateGraphics();

            if (_editmode)
            {
                int drawTopLeftMark = 12;
                if (!_showEdit) drawTopLeftMark = 6;

                int drawTopRightMark = 12;
                if (!_showDelete) drawTopRightMark = 6;

                int drawBottomRightMark = 1;
                if (!_showResize) drawBottomRightMark = 6;

                int drawBottomLeftMark = 6;

                //draw top border
                formGraphics.DrawLine(myPen, drawTopLeftMark, 1, this.Width - drawTopRightMark, 1);

                //draw right border
                formGraphics.DrawLine(myPen, this.Width - 1, drawTopRightMark, this.Width - 1, this.Height - drawBottomRightMark);

                //draw bottom border
                formGraphics.DrawLine(myPen, drawBottomLeftMark, this.Height - 1, this.Width - drawBottomRightMark, this.Height - 1);

                //draw left border
                formGraphics.DrawLine(myPen, 1, drawTopLeftMark, 1, this.Height - drawBottomLeftMark);


                
                
                System.Reflection.Assembly thisExe;
                thisExe = System.Reflection.Assembly.GetExecutingAssembly();

                System.IO.Stream pic;

                //TOP LEFT MARKER
                if (_showEdit)
                {
                    pic = thisExe.GetManifestResourceStream("Dashboard.edit.png");
                    formGraphics.DrawImage(Image.FromStream(pic), 0, 0);
                }
                else { formGraphics.DrawLine(myPen, drawTopLeftMark, 1, 1, drawTopLeftMark); }

                //TOP RIGHT MARKER
                if (_showDelete)
                {
                    pic = thisExe.GetManifestResourceStream("Dashboard.delete.png");
                    formGraphics.DrawImage(Image.FromStream(pic), this.Width - 24, 0);
                }
                else { formGraphics.DrawLine(myPen, this.Width - drawTopRightMark, 0, this.Width, drawTopRightMark); }

                //BOTTOM RIGHT MARKER
                if (_showResize)
                {
                    pic = thisExe.GetManifestResourceStream("Dashboard.resize.png");
                    formGraphics.DrawImage(Image.FromStream(pic), this.Width - 15, this.Height - 15);
                }
                else { formGraphics.DrawLine(myPen, this.Width - drawBottomRightMark-1, this.Height , this.Width, this.Height - drawBottomRightMark-1); }
                
                //BOTTOM LEFT MARKER
                formGraphics.DrawLine(myPen, 0, this.Height - 7, 6, this.Height - 1);


            }
            else
            {
                //formGraphics.Clear(Color.Black);
            }


            //myPen.Dispose();
            formGraphics.Dispose();
        }
       

        //Closing Module event
        public delegate void NewCloseModuleDelegate(object sender, EventArgs e);
        public event NewCloseModuleDelegate OnCloseModule;
        protected void CloseModule(object sender, EventArgs e)
        {
            if (OnCloseModule != null) OnCloseModule(sender, e);
        }



        #region "Moving Module"
        bool isMovingModule = false;
        bool isResizingModule = false;
        int moveLeft, moveTop;
        int resizeWidth, resizeHeight;

        public int MouseX = 50;
        public int MouseY = 50;

        public int minimumWidth = 150;
        public int minimumHeight = 100;

        public delegate void NewMoveModuleDelegate(object sender, EventArgs e);
        public event NewMoveModuleDelegate OnMoveModule;
        protected void MoveModule(object sender, EventArgs e)
        {
            if (!EditMode) return;
            if (OnMoveModule != null) OnMoveModule(sender, e);
        }

        public virtual void modMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!EditMode) return;


            if (e.X < 24 && e.Y < 24 && _showEdit)
            {
                //MessageBox.Show("Show Edit Dialog");
            }
            else if (e.X > this.Width - 24 && e.Y < 24 && _showDelete)
            {
                //MessageBox.Show("Delete Module");
            }
            else if (e.X > this.Width - 15 && e.Y > this.Height - 15 && _showResize) //bottom right corner...
            {
                isResizingModule = true;
                moveLeft = this.Width + (this.Width - e.X);
                moveTop = this.Height + (this.Height - e.Y);
                resizeWidth = this.Width;
                resizeHeight = this.Height;
            }
            else
            {
                moveLeft = e.X;
                moveTop = e.Y;
                isMovingModule = true;
            }

            
        }

        public virtual void modMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!EditMode) return;

            
            //SNAP the module to the grid
            if (isMovingModule)
            {  //autocorrect top/left
            
                //foreach (Point p in MAIN.Grid)
                //{
                //    if (this.Left >= p.X - 25 && this.Left <= p.X + 25) this.Left = p.X;
                //    if (this.Top >= p.Y - 25 && this.Top <= p.Y + 25) this.Top = p.Y;
                //}


                ////now determine what the grid row/column of this position is...
                //GRID_Column = (this.Left - MAIN.Grid[0].X) / 50;
                //GRID_Row = (this.Top - MAIN.Grid[0].Y) / 50;
                //Title = GRID_Column.ToString() + " x " + GRID_Row.ToString();


            }
            else if (isResizingModule && _showResize)
            {  //autocorrect width/height
                foreach (Point p in MAIN.Grid)
                {
                    if (this.Left + this.Width >= p.X - 25 && this.Left + this.Width <= p.X + 25) this.Width = p.X - this.Left;
                    if (this.Top + this.Height >= p.Y - 25 && this.Top + this.Height <= p.Y + 25) this.Height = p.Y - this.Top;
                }
            }
            else
            {
                if (e.X < 24 && e.Y < 24 && _showEdit)
                {
                    MessageBox.Show("Show Edit Dialog");
                }
                else if (e.X > this.Width - 24 && e.Y < 24 && _showDelete)
                {
                    //MessageBox.Show("Delete Module");

                    DialogResult res = MessageBox.Show("Are you sure?", "Remove Component", MessageBoxButtons.YesNoCancel);
                    if (res == DialogResult.Yes)
                    {
                        MAIN.Controls.Remove(this);
                    }

                }
            }
           
            MoveModule(this, null);

            isMovingModule = false;
            isResizingModule = false;
        }



        //stores the top/left grid that this module belongs to.
        int _row = 0;
        public int GRID_Row
        {
            get { return _row; }
            set { _row = value; }
        }

        int _column = 0;
        public int GRID_Column
        {
            get { return _column; }
            set { _column = value; }
        }

        bool _refreshed = false;
        public bool Refreshed
        {
            get { return _refreshed; }
            set { _refreshed = value; }
        }
        bool _nostatus = false;
        public bool NoStatus
        {
            get { return _nostatus; }
            set {
                if (value)
                {
                    _nostatus = value;
                    this.Refresh();
                }

            }
        }



        public virtual void modMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MouseX = e.X;
            MouseY = e.Y;

            //Manipulate module
            if (isMovingModule)
            {  //change location
                if (this.Left + (e.X - moveLeft) >= (this.Parent.Width- SystemInformation.Border3DSize.Width) - this.Width - MAIN.Grid[0].X)
                {
                    this.Left = (this.Parent.Width- SystemInformation.Border3DSize.Width) - this.Width - MAIN.Grid[0].X-6;
                }
                else if (this.Left + (e.X - moveLeft) <= MAIN.Grid[0].X)
                {
                    this.Left = MAIN.Grid[0].X;
                }
                else
                {
                    //this.Left = this.Left + (e.X - moveLeft);

                    foreach (Point p in MAIN.Grid)
                    {
                        if ((this.Left + (e.X - moveLeft)) >= p.X - 25 && (this.Left + (e.X - moveLeft)) <= p.X + 25) { this.Left = p.X; break; }
                        //if (this.Top >= p.Y - 25 && this.Top <= p.Y + 25) this.Top = p.Y;
                    }
                    
                }

                if (this.Top + (e.Y - moveTop) >= this.Parent.Height - this.Height - MAIN.Grid[0].Y)
                {
                    this.Top = this.Parent.Height - this.Height - MAIN.Grid[0].Y+2;
                }
                else if (this.Top + (e.Y - moveTop) <= MAIN.Grid[0].Y)
                {
                    this.Top = MAIN.Grid[0].Y;
                }
                else
                {
                    //this.Top = this.Top + (e.Y - moveTop);
                    foreach (Point p in MAIN.Grid)
                    {
                        //if ((this.Left + (e.X - moveLeft)) >= p.X - 25 && (this.Left + (e.X - moveLeft)) <= p.X + 25) { this.Left = p.X; break; }
                        if ((this.Top + (e.Y - moveTop)) >= p.Y - 25 && (this.Top + (e.Y - moveTop)) <= p.Y + 25) { this.Top = p.Y; break; }
                    }
                }


                GRID_Column = (this.Left - MAIN.Grid[0].X) / 50;
                GRID_Row = (this.Top - MAIN.Grid[0].Y) / 50;
                //Title = GRID_Column.ToString() + " x " + GRID_Row.ToString();
            }
            else if (isResizingModule)
            {  //change size

                if ((e.X + moveLeft - resizeWidth) > minimumWidth)
                    this.Width = (e.X + moveLeft - resizeWidth);
                else this.Width = minimumWidth;
                if ((e.Y + moveTop - resizeHeight) > minimumHeight)
                    this.Height = (e.Y + moveTop - resizeHeight);
                else this.Height = minimumHeight;
            }
            
        }
        #endregion


        public virtual void ConfigureProperty(string PropertyName, string value)
        {
            switch (PropertyName)  //Module - generic properties
            {
                case "title": this.Title = value; break;
                case "frequency": this.Frequency = int.Parse(value); break;
                case "top": this.Top = int.Parse(value); break;
                case "left": this.Left = int.Parse(value); break;
                case "width": this.Width = int.Parse(value); break;
                case "height": this.Height = int.Parse(value); break;
            }
        }



    }
}
