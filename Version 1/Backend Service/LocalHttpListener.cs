﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;
using System.Xml;

namespace BFD
{
    class LocalHttpListener
    {
        public event MsgHandler EventLog;
        public delegate void MsgHandler(string message, EventArgs e);
        void callEventLog(string message, EventArgs e)
        {
            try
            {
                if (EventLog != null && DEBUG) EventLog(message, e);
            }
            catch (Exception ex)
            {
                EventLog(ex.Message, e);
            }
        }


        BFAStatus stat;

        public const string UriAddress = "http://+";
        HttpListener _httpListener;
        string port = "3001";

        public LocalHttpListener()
        {
            _httpListener = new HttpListener();
        }



        string DefaultCMD = "";  //by default we will not respond to a no-soap msg command via http
        bool DEBUG = false;
        public void Start()
        {
            stat = new BFAStatus();  //create a status class which constantly collects the status of various items as configured.
            stat.EventLog += new BFAStatus.MsgHandler(callEventLog);

            //if (Msg != null) Msg("Listening to : " + UriAddress, null);
            //Pull registry configuration settings
            try
            {
                using (RegistryKey rConfig = Registry.LocalMachine.OpenSubKey(@"Software\BigFix\Dashboard"))
                {
                    if (rConfig.ValueCount > 0)
                    {
                        foreach (string valueName in rConfig.GetValueNames())
                        {
                            switch (valueName.ToLower())
                            {
                                case "defaultcmd":
                                    try { DefaultCMD = rConfig.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "wrurl":
                                    try { stat.WRUrl = rConfig.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "wrpassword":
                                    try { stat.WRPassword = rConfig.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "wrusername":
                                    try { stat.WRUsername = rConfig.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "debug":
                                    try { if (rConfig.GetValue(valueName).ToString().ToLower() == "true") DEBUG = true; }
                                    catch { }
                                    break;
                                case "port":
                                    try { port = rConfig.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                            }
                        }
                    }
                }
            }
            catch { }

            try
            {
                callEventLog("Starting Listener with following Uri: " + LocalHttpListener.UriAddress + ":" + port + "/", null);

                _httpListener.Prefixes.Add(LocalHttpListener.UriAddress + ":" + port + "/");
                _httpListener.Start();
            }
            catch (Exception ex)
            {
                callEventLog("ERROR: " + ex.Message, null);
            }
            stat.Start();
            

            while (_httpListener.IsListening)
                ProcessRequest();
        }

        

        public void Stop()
        {
            //if (Msg != null) Msg("Listener Stopped", null);
            stat.Stop();
            _httpListener.Stop();
        }


        //this function is called and triggers the ListenerCallback if something is received.
        void ProcessRequest()
        {
            var result = _httpListener.BeginGetContext(ListenerCallback, _httpListener);
            result.AsyncWaitHandle.WaitOne();
        }






        //Triggered when the client sends in the request
        void ListenerCallback(IAsyncResult result)
        {
            var context = _httpListener.EndGetContext(result);
            var info = Read(context.Request);

            //--------------------------
            //TODO
            //parse incoming file to determine what commands are present...
            //for now always assume they're wanting the current status.
            string CMD = DefaultCMD;
            string Args = "";

            string Username = "";
            string Password = "";

            string XML = info.Body;

            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(XML)))
                {
                    //first grab command and arguments
                    if (reader.ReadToFollowing("Command"))
                    {
                        if (reader.MoveToAttribute("action")) CMD = reader.Value;
                        if (reader.MoveToAttribute("arguments")) if (reader.HasValue) Args = reader.Value;
                    }

                    //next grab credentials
                    if (reader.ReadToFollowing("Credentials"))
                    {
                        if (reader.MoveToAttribute("username")) Username = reader.Value;
                        if (reader.MoveToAttribute("password")) Password = reader.Value;
                    }

                    //reader.ReadElementContentAsString()
                }
            }
            catch { }
            
            //the command used can be pulled from code, guid, could even include a username/password so we don't just GIVE the stuff away
            //--------------------------
            string CMDResponse = "";


            //if (Authenticate(Username, Password))
            //{
                callEventLog(CMD + "(" + Args + ")", null);


                //perform an action based on command received
                switch (CMD.ToLower())
                {
                    case "status":
                        CMDResponse = stat.ToString();
                        break;

                    case "relevance":
                        try
                        {
                            CMDResponse = WebReports_Post(stat.WRUrl, stat.WRUsername, stat.WRPassword, Args);
                        }
                        catch (Exception ex)
                        {
                            //CMDResponse = "ERROR: " + EscapeString(ex.Message);
                            CMDResponse = "ERROR: Syntax error in your relevance statement, please correct and try again.";
                        }
                        break;

                    case "servicestop":
                        if (Authenticate(Username, Password))
                            CMDResponse = Service_Action(Args, "stop");
                        else CMDResponse = "ERROR: Request denied, unauthorized action.";
                        break;

                    case "servicestart":
                        if (Authenticate(Username, Password))
                            CMDResponse = Service_Action(Args, "start");
                        else CMDResponse = "ERROR: Request denied, unauthorized action.";
                        break;

                }

                callEventLog("Answer: " + CMDResponse, null);
            //}

            //TODO
            //consider encrypting the response so it's not over the free air.


            //THIS IS HOW TO SEND A RESPONSE TO THE CLIENT
            CreateResponse(context.Response, CMDResponse);
        }


        private bool Authenticate(string username, string password)
        {
            if (username == "Daniel" && password == "password")
                return true;
            else return false;
        }




        public string Service_Action(string Title, string Action)
        {
            string ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

            ret += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + Environment.NewLine;
            ret += "    <soapenv:Body>" + Environment.NewLine;
            ret += "        <ServiceAction xmlns=\"http://schemas.bigfix.com/Relevance\">" + Environment.NewLine;


            ret += "<a>";

            switch (Action.ToLower())
            {
                case "start":
                    ret += EscapeString(stat.StartService(Title));
                    break;
                case "stop":
                    ret += EscapeString(stat.StopService(Title));
                    break;
                default:
                    ret += "Unknown Service Command";
                    break;
            }

            ret += "</a>" + Environment.NewLine;



            ////loop objects and get xml status
            //foreach (string a in answers)
            //{
            //    ret += "<a>" + a + "</a>";
            //}

            ret += "        </ServiceAction>" + Environment.NewLine;
            ret += "    </soapenv:Body>" + Environment.NewLine;
            ret += "</soapenv:Envelope>" + Environment.NewLine;

            return ret;
        }




        //this is more complicated than the function within BFAStatus.
        //This function needs to return a completely formated XML response
        //if multiple answers, then encapsolate in multiple <a></a> markers
        public string WebReports_Post(string WRURL, string WRUsername, string WRPassword, string Relevance)
        {
            WebServiceInvoker invoker = new WebServiceInvoker(new Uri(WRURL));

            string service = "RelevanceBinding";
            string method = "GetRelevanceResult";

            string[] args = new string[] { Relevance, WRUsername, WRPassword };

            string[] answers = invoker.InvokeMethod<string[]>(service, method, args);



            string ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;

            ret += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + Environment.NewLine;
            ret += "    <soapenv:Body>" + Environment.NewLine;
            ret += "        <GetRelevanceResultResponse xmlns=\"http://schemas.bigfix.com/Relevance\">" + Environment.NewLine;

            //loop objects and get xml status
            foreach (string a in answers)
            {
                ret += "<a>" + EscapeString(a) + "</a>" + Environment.NewLine;
            }

            ret += "        </GetRelevanceResultResponse>" + Environment.NewLine;
            ret += "    </soapenv:Body>" + Environment.NewLine;
            ret += "</soapenv:Envelope>" + Environment.NewLine;

            return ret;
        }










        public string EscapeString(string str)
        {
            str = str.Replace("&", "&amp;");
            str = str.Replace("\"", "&quot;");
            str = str.Replace("'", "&apos;");
            str = str.Replace("<", "&lt;");
            str = str.Replace(">", "&gt;");
            return str;
        }












        public static WebRequestInfo Read(HttpListenerRequest request)
        {
            var info = new WebRequestInfo();
            info.HttpMethod = request.HttpMethod;
            info.Url = request.Url;

            if (request.HasEntityBody)
            {
                Encoding encoding = request.ContentEncoding;
                using (var bodyStream = request.InputStream)
                using (var streamReader = new StreamReader(bodyStream, encoding))
                {
                    if (request.ContentType != null)
                        info.ContentType = request.ContentType;

                    info.ContentLength = request.ContentLength64;
                    info.Body = streamReader.ReadToEnd();
                }
            }

            return info;
        }

        public static WebResponseInfo Read(HttpWebResponse response)
        {
            var info = new WebResponseInfo();
            info.StatusCode = response.StatusCode;
            info.StatusDescription = response.StatusDescription;
            info.ContentEncoding = response.ContentEncoding;
            info.ContentLength = response.ContentLength;
            info.ContentType = response.ContentType;

            using (var bodyStream = response.GetResponseStream())
            using (var streamReader = new StreamReader(bodyStream, Encoding.UTF8))
            {
                info.Body = streamReader.ReadToEnd();
            }

            return info;
        }

        private static void CreateResponse(HttpListenerResponse response, string body)
        {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.StatusDescription = HttpStatusCode.OK.ToString();
            byte[] buffer = Encoding.UTF8.GetBytes(body);
            response.ContentLength64 = buffer.Length;
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.OutputStream.Close();
        }
    }






    public class WebRequestInfo
    {
        public string Body { get; set; }
        public long ContentLength { get; set; }
        public string ContentType { get; set; }
        public string HttpMethod { get; set; }
        public Uri Url { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("HttpMethod {0}", HttpMethod));
            sb.AppendLine(string.Format("Url {0}", Url));
            sb.AppendLine(string.Format("ContentType {0}", ContentType));
            sb.AppendLine(string.Format("ContentLength {0}", ContentLength));
            sb.AppendLine(string.Format("Body {0}", Body));
            return sb.ToString();
        }
    }

    public class WebResponseInfo
    {
        public string Body { get; set; }
        public string ContentEncoding { get; set; }
        public long ContentLength { get; set; }
        public string ContentType { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string StatusDescription { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("StatusCode {0} StatusDescripton {1}", StatusCode, StatusDescription));
            sb.AppendLine(string.Format("ContentType {0} ContentEncoding {1} ContentLength {2}", ContentType, ContentEncoding, ContentLength));
            sb.AppendLine(string.Format("Body {0}", Body));
            return sb.ToString();
        }
    }
}
