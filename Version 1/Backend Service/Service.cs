﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Xml.Serialization;


namespace BFD
{
    public partial class Service : ServiceBase
    {
        public Service()
        {
            InitializeComponent();
        }

        LocalHttpListener server;
            
        protected override void OnStart(string[] args)
        {
            server = new LocalHttpListener();

            server.EventLog += new LocalHttpListener.MsgHandler(callEventLog);

            var task = Task.Factory.StartNew(() => server.Start());
        }

       
        protected override void OnStop()
        {
            server.Stop();
        }

        void callEventLog(string message, EventArgs e)
        {
            EventLog.WriteEntry(message);
        }





        private static void SetRequestBody(WebRequest webRequest, string body)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(body);
            webRequest.ContentLength = buffer.Length;
            using (Stream requestStream = webRequest.GetRequestStream())
                requestStream.Write(buffer, 0, buffer.Length);
        }

        private static string ConvertToXmlString(object body)
        {
            string xmlBody;
            var xmlSerializer = new XmlSerializer(body.GetType());
            using (StringWriter writer = new StringWriter())
            {
                xmlSerializer.Serialize(writer, body);
                xmlBody = writer.ToString();
            }
            return xmlBody;
        }

    }
}
