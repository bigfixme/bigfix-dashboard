﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.ServiceProcess;
using System.Threading;
using System.Net;
using System.IO;
using System.Data.SqlClient;

namespace BFD
{
    class BFAStatus
    {
        public event MsgHandler EventLog;
        public delegate void MsgHandler(string message, EventArgs e);
        public void callEventLog(string message, EventArgs e)
        {
            if (EventLog != null) EventLog(message, e);
        }

        List<BFA> objects = new List<BFA>();
        
        public void Start()
        {
            //add a module whose job is to read in new modules as needed
            //this will configure a timer to read the reg every 15min for new modules
            BFA svc = new BFA(@"Software\BigFix\Dashboard\Settings", this);
            svc.CheckforNewComponentsmodule = true; //this is the only module configured this way!
            objects.Add(svc);
        }

        public void Stop()
        {
        }



        /// <summary>
        /// Finds the appropriate component and forwards the Start command
        /// </summary>
        /// <param name="Title">Title of the service component we are starting.</param>
        /// <returns></returns>
        public string StartService(string Title)
        {
            foreach (BFA obj in objects)
            {
                if (obj.Title == Title && obj.Type == "service")
                {
                    return obj.StartService();
                }
            }
            return "ERROR: Unable to find Service with title='" + Title + "'.";
        }

        /// <summary>
        /// Finds the appropriate component and forwards the Stop command
        /// </summary>
        /// <param name="Title">Title of the service component we are stopping.</param>
        /// <returns></returns>
        public string StopService(string Title)
        {
            foreach (BFA obj in objects)
            {
                if (obj.Title == Title)
                {
                    return obj.StopService();
                }
            }
            return "ERROR: Unable to find Service with title='" + Title + "'.";
        }



        string _wrurl = "";
        public string WRUrl
        {
            get { return _wrurl; }
            set { _wrurl = value; }
        }
        string _wrusername = "";
        public string WRUsername
        {
            get { return _wrusername; }
            set { _wrusername = value; }
        }
        string _wrpassword = "";
        public string WRPassword
        {
            get { return _wrpassword; }
            set { _wrpassword = value; }
        }





        /// <summary>
        /// Read in the settings and all their sub-values.
        /// </summary>
        /// <param name="regKeyPath">Registry path to this Setting.</param>
        /// <param name="svc">Leave null to create a new object.</param>
        public bool ReadSetting(string regKeyPath, BFA svc = null)
        {
            try
            {
                using (RegistryKey rValues = Registry.LocalMachine.OpenSubKey(regKeyPath))
                {
                    if (rValues.ValueCount > 0)
                    {
                        bool addtoList = false;

                        if (svc == null)
                        {
                            //we don't want to create duplicates, so we need to 
                            //confirm this regKeyPath doesn't already exist... 
                            //if so just return immediately
                            foreach (BFA sv in objects)
                            {
                                if (sv.regKeyPath == regKeyPath) return false;
                            }

                            string[] cn = regKeyPath.Split(new char[] { '\\' });

                            callEventLog("Adding New Component:  " + cn[cn.Length - 1], null);
                            addtoList = true;
                            svc = new BFA(regKeyPath, this);
                        }
                        else
                        {
                            callEventLog("Updating Component Configuration", null);
                        }

                        //pull type of setting this is from the default value of this key
                        try { svc.Type = rValues.GetValue(null).ToString().ToLower(); }
                        catch { }

                        //now pull in all the various settings for this object
                        foreach (string valueName in rValues.GetValueNames())
                        {
                            switch (valueName.ToLower())
                            {
                                case "category":
                                    try { svc.Category = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "title":
                                    try { svc.Title = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "servicename":
                                    try { svc.ServiceName = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "desc":
                                    try { svc.Desc = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "readonly":
                                    try { if (rValues.GetValue(valueName).ToString().ToLower() == "false") svc.ReadOnly = false; else svc.ReadOnly = true; }
                                    catch { }
                                    break;
                                case "machine":
                                    try { svc.Machine = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "relevance":
                                    try { svc.Relevance = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "interval":
                                    try { svc.Interval = Int32.Parse(rValues.GetValue(valueName).ToString()); }
                                    catch { }
                                    break;
                                case "status":
                                    try { svc.Status = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "key":
                                    try { svc.Key = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "value":
                                    try { svc.Value = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "accesskey":
                                    try { svc.AccessKey = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "streamname":
                                    try { svc.StreamName = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "hidden":
                                    try { if (rValues.GetValue(valueName).ToString().ToLower() == "false") svc.Hidden = false; else svc.Hidden = true; }
                                    catch { }
                                    break;
                                case "sqlcmd":
                                    try { svc.SQLCmd = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "sqlserver":
                                    try { svc.SQLServer = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                                case "sqldatabase":
                                    try { svc.SQLDatabase = rValues.GetValue(valueName).ToString(); }
                                    catch { }
                                    break;
                            }
                        }

                        //done pulling in values, so add to objects list
                        if (addtoList) objects.Add(svc);

                    } //else no settings to be configured

                }

                return true;
            }
            catch (Exception ex)
            {
                callEventLog("ERROR: " + ex.Message, null);
                return true;
            }
        }



        /// <summary>
        /// Converts this object into an XML file containing a list of all registerd components.
        /// </summary>
        /// <returns>XML document</returns>
        public override string ToString()
        {
            string ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;

            ret += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + Environment.NewLine;
            ret += "    <soapenv:Body>" + Environment.NewLine;
            ret += "        <status xmlns=\"http://schemas.bigfix.me/Status\">" + Environment.NewLine;

            //loop objects and get xml status
            foreach (BFA obj in objects)
            {
                ret += obj.ToString();
            }

            //add a timestamp so client knows when this status was valid
            ret += "            <component title=\"Last Update\" type=\"other\" status=\"" + EscapeString(DateTime.Now.ToString()) + "\" category=\"other\">This is the timestamp of the information you are viewing.  Return to the Status page and pull down to refresh data.</component>" + Environment.NewLine;

            ret += "        </status>" + Environment.NewLine;
            ret += "    </soapenv:Body>" + Environment.NewLine;
            ret += "</soapenv:Envelope>" + Environment.NewLine;


            return ret;
        }

        /// <summary>
        /// Parameterizes the string to it functions properly with XML
        /// </summary>
        /// <param name="str">String to be 'repaired'</param>
        /// <returns>Repaired string</returns>
        public string EscapeString(string str)
        {
            if (str == null) return null;

            str = str.Replace("&", "&amp;");
            str = str.Replace("\"", "&quot;");
            //str = str.Replace("'", "&apos;");
            str = str.Replace("<", "&lt;");
            str = str.Replace(">", "&gt;");
            return str;
        }






    }







    //structure used to monitor a service.
    class BFA
    {
        System.Threading.Timer timer;

        public BFAStatus _parent = null;


        string _regKeyPath = "";
        public string regKeyPath
        {
            get { return _regKeyPath; }
            set { _regKeyPath = value; }
        }


        public BFA(string _KeyPath, BFAStatus _Parent = null)
        {
            _parent = _Parent;
            _regKeyPath = _KeyPath;

            //setup thread timer to poll for the status of this service
            System.Threading.TimerCallback cb = new System.Threading.TimerCallback(Tick);

            clsTimer time = new clsTimer();
            timer = new System.Threading.Timer(cb, time, 15000,_interval); //wait 10sec then trigger every 15min
            
        }

        /// <summary>
        /// this is the guts of the object... the tick is triggered and we need to take action
        /// </summary>
        /// <param name="obj">clsTimer object</param>
        private void Tick(object obj)
        {
            if (obj is clsTimer)
            {
                clsTimer t = (clsTimer)obj;

                
                //before we "do" anything, let's read in the registry for this object incase the user changed something last minute.
                if (_checkfornewcomponentsmodule)
                {
                    _parent.callEventLog("Reading Settings " + _interval.ToString(), null);

                    //--------------------------------
                    //PULL IN SERVICES TO BE MONITORED
                    try
                    {
                        using (RegistryKey rSettings = Registry.LocalMachine.OpenSubKey(_regKeyPath))
                        {
                            foreach (string SettingName in rSettings.GetSubKeyNames())
                            {
                                _parent.ReadSetting(_regKeyPath + @"\" + SettingName);
                            }
                        }
                    }
                    catch { }
                    //--------------------------------

                    return;  //nothing more to do for this type of module (ie: new component checker)
                }
                else
                {
                    _deleted = !(_parent.ReadSetting(_regKeyPath, this));
                    if (_deleted)
                        _parent.callEventLog("Deleted " + _regKeyPath, null);
                    else _parent.callEventLog(_type + " componet " + _title + " was triggered by the timer.  " + _interval.ToString(), null);
                }

                if (_deleted) return; //user deleted this item out of the registry... nothing to do.

                _errmsg = ""; string LeftronicPoint = "";
                switch (_type)  //do something based on the type of object this is.
                {
                    ///----------------------------------------
                    ///Component:  REGISTRY
                    ///Description:  This object reads a particular part of the registry as defined by the key and value properties.
                    case "registry":
                        if (_key != "" && _value != "")
                        {
                            _status = t.GetRegistryValue(_key, _value);
                            LeftronicPoint = _status;
                        }
                        else
                        {
                            _status = "ERROR";
                            _errmsg = "Registry Component not propertly configured, key='" + _key + "'  value='" + _value + "'.";
                        }
                        break;


                    ///----------------------------------------
                    /// Component:  RELEVANCE
                    /// Description:  This type of object calls the web-reports server and runs a piece of session relevance.
                    /// due to the nature of this component, we can only return the first item in the results
                    case "relevance":
                        //these are customized functions we can call that are built into this app.  Currently I do not have anything like this

                        //if this component has "relevance" then we'll need to make a call to the web-reports server
                        if (_relevance != "")
                        {
                            try
                            {
                                _status = t.WebReports_Post(_parent.WRUrl, _parent.WRUsername, _parent.WRPassword, _relevance)[0];
                                LeftronicPoint = _status;
                            }
                            catch (Exception ex)
                            {
                                _status = "ERROR";
                                _errmsg = ex.Message + "   " + _relevance;
                            }
                        }
                        break;

                    ///----------------------------------------
                    ///Component:  SERVICE
                    ///Description:  This type of component queries windows for the status of the service.
                    ///for integration with Leftronic, we set running = 0 (default for green), stopped = 100 (default for red) and 
                    ///everything else gets a 50 (which is yellow)
                    case "service":
                        if (_machine != "" && _servicename != "")
                        {
                            _status = t.GetServiceStatus(_servicename, _machine);

                            switch (_status)
                            {
                                case "Running": LeftronicPoint = "0"; break;
                                case "Stopped": LeftronicPoint = "100"; break;
                                default: LeftronicPoint = "50"; break;
                            }
                        }
                        else
                        {
                            _status = "ERROR";
                            _errmsg = "Service Component not properly configured, machine='" + _machine + "'  servicename='" + _servicename + "'.";
                        }
                        break;


                    ///----------------------------------------
                    ///Component:  STATUS
                    ///Description:  this object simply reads the "status" value from the registry.
                    case "status":
                        LeftronicPoint = _status;
                        break;


                    ///----------------------------------------
                    ///Component:  SQL
                    ///Description:  This object will create a sql connection to the specified server and run the sql cmd.
                    case "sql":
                        _status = t.SQLServer_Post(_sqlcmd, _sqlserver, _sqldatabase);
                        if (_status.StartsWith("ERROR") || _status.StartsWith("SQL ERROR") || _status.StartsWith("POST ERROR"))
                        {
                            _errmsg = _status;
                            _status = "ERROR";
                        }
                        else { _errmsg = ""; }
                        break;


                    ///----------------------------------------
                    ///Component: OTHER
                    ///Description:  this is basically a type where we don't have programmed yet.  So nothing will occur here.
                    case "other":
                        //these are user controlled and allow admins to configure the ability to change things.
                        break;
                }

                if (_status.StartsWith("ERROR:"))
                { //error condition has occured...
                    _errmsg = _status.Split(new char[] { ':' }, 2)[1];
                    _status = "ERROR";
                }


                //indicate in the registry when we last checked the status of this object.
                t.SetRegistryValue(_regKeyPath, "Last Updated", DateTime.Now.ToString());
                t.SetRegistryValue(_regKeyPath, "Status", _status);
                if (_errmsg != "") t.SetRegistryValue(_regKeyPath, "Error", _errmsg);


                //send value out to Leftronic if necessary
                if (_accesskey != "" && _streamname != "" && LeftronicPoint != "")
                {
                    try
                    {
                        string ret = t.Leftronic_Post("{\"accessKey\": \"" + _accesskey + "\", \"streamName\": \"" + _streamname + "\", \"point\": " + LeftronicPoint + " }");
                        if (!ret.Contains("OK"))
                            _leftronicstatus = "ERROR: " + ret;
                        else _leftronicstatus = "";
                        //_parent.callEventLog("TICK:  " + ret, null);
                    }
                    catch (Exception ex)
                    {
                        _leftronicstatus = "ERROR: " + ex.Message;
                        //_parent.callEventLog("TICK:  " + ex.Message, null);
                    }

                    t.SetRegistryValue(_regKeyPath, "Leftronic Status", _leftronicstatus);
                }



            }
        }




        /// <summary>
        /// Interval allows the user to change the tick frequency
        /// Default: 15min
        /// </summary>
        Int32 _interval = 900000;
        public Int32 Interval
        {
            get { return _interval; }
            set
            {
                _interval = value;
                timer.Change(value, value);
            }
        }



        //this is a flag which configures a special module to check for "new" modules without having to restart hte service.
        bool _checkfornewcomponentsmodule = false;
        public bool CheckforNewComponentsmodule
        {
            get { return _checkfornewcomponentsmodule; }
            set { _checkfornewcomponentsmodule = value; _hidden = true; }
        }





        string _category = "other";
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }


        string _key = "";
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        string _value = "";
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        string _relevance = "";
        public string Relevance
        {
            get { return _relevance; }
            set { _relevance = value; }
        }
        


        //customization for the Leftronic Dashboards
        string _accesskey = "";
        public string AccessKey
        {
            get { return _accesskey; }
            set { _accesskey = value; }
        }
        string _streamname = "";
        public string StreamName
        {
            get { return _streamname; }
            set { _streamname = value; }
        }
        string _leftronicstatus = "";
        public string LeftronicStatus
        {
            get { return _leftronicstatus; }
            set { _leftronicstatus = value; }
        }


        string _errmsg = "";

        string _status = "Reading";
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }


        string _sqlserver = "localhost";
        public string SQLServer
        {
            get { return _sqlserver; }
            set { _sqlserver = value; }
        }
        string _sqlcmd = "";
        public string SQLCmd
        {
            get { return _sqlcmd; }
            set { _sqlcmd = value; }
        }
        string _sqldatabase = "BFEnterprise";
        public string SQLDatabase
        {
            get { return _sqldatabase; }
            set { _sqldatabase = value; }
        }

        /// <summary>
        /// Name of this object.  Used in a variety of ways.
        /// </summary>
        string _servicename = "";
        public string ServiceName
        {
            get { return _servicename; }
            set { _servicename = value; }
        }


        string _title = "";
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        /// <summary>
        /// Type = general, service, etc (other)... 
        /// </summary>
        string _type = "";
        public string Type
        {
            get { return _type; }
            set { _type = value.ToLower(); }
        }




        /// <summary>
        /// This is a flag if the user deleted the registry key related to this component.
        /// </summary>
        bool _deleted = false;
        public bool Deleted  //determines if the service can be controlled remotely
        {
            get { return _deleted; }
            set { _deleted = value; }
        }
        

        /// <summary>
        /// Hidden lets you decide if it is shared via the XML feed or not.
        /// this would typically be used if you want a second feed to Leftronic.com
        /// </summary>
        bool _hidden = false;
        public bool Hidden  //determines if the service can be controlled remotely
        {
            get { return _hidden; }
            set { _hidden = value; }
        }
        
        /// <summary>
        /// ReadOnly determines if we allow the user to remotely modify the state.
        /// </summary>
        bool _readonly = true;
        public bool ReadOnly  //determines if the service can be controlled remotely
        {
            get { return _readonly; }
            set { _readonly = value; }
        }

        
        /// <summary>
        /// Machine indicates what the target machine is to perform a particular action.
        /// </summary>
        string _machine = "localhost";
        public string Machine
        {
            get { return _machine; }
            set { _machine = value; }
        }       

        

        /// <summary>
        /// Desc is a general description of this object.  Like name, used in a variety of ways.
        /// </summary>
        string _desc = "";
        public string Desc
        {
            get { return _desc; }
            set { _desc = value; }
        }



        /// <summary>
        /// Convert this object to an XML string we can easily send to users.
        /// </summary>
        /// <returns>XML component</returns>
        public override string ToString()
        {
            if (_hidden || _deleted) return "";


            string ret = "";

            ret += "<component title=\"" + _parent.EscapeString(_title) + "\" type=\"" + _parent.EscapeString(_type) + "\" status=\"" + _parent.EscapeString(_status) + "\" category=\"" + _parent.EscapeString(_category) + "\">" + Environment.NewLine;
            
            //if no errors occured, then send description, else forward the error message for troubleshooting
            if (_status == "ERROR")
                ret += _parent.EscapeString(_errmsg);
            else ret += _parent.EscapeString(_desc);

            if (_servicename != "") ret += "    <servicename>" + _parent.EscapeString(_servicename) + "</servicename>" + Environment.NewLine;
            if (!_readonly) ret += "    <readonly>" + _readonly.ToString() + "</readonly>" + Environment.NewLine;  //only send if user can edit this component
            if (_machine != "localhost" && _machine != "") ret += "    <machine>" + _parent.EscapeString(_machine) + "</machine>" + Environment.NewLine; //send only if it's not the local host
            if (_interval != 900000) ret += "    <interval>" + _interval.ToString() + "</interval>" + Environment.NewLine; //send if not the default 15min timer
            if (_key != "") ret += "    <key>" + _parent.EscapeString(_key) + "</key>" + Environment.NewLine;  //send if not blank
            if (_value != "") ret += "    <value>" + _parent.EscapeString(_value) + "</value>" + Environment.NewLine;  //send if not blank
            if (_leftronicstatus != "") ret += "    <LeftronicStatus>" + _parent.EscapeString(_leftronicstatus) + "</LeftronicStatus>" + Environment.NewLine;

            if (_relevance != "") ret += "    <relevance>" + _parent.EscapeString(_relevance) + "</relevance>" + Environment.NewLine;  //send if not blank
            if (_sqlcmd != "") ret += "    <sqlcmd>" + _parent.EscapeString(_sqlcmd) + "</sqlcmd>" + Environment.NewLine;  //send if not blank
            if (_sqlserver != "" && _sqlserver != "localhost") ret += "    <sqlserver>" + _parent.EscapeString(_sqlserver) + "</sqlserver>" + Environment.NewLine;  //send if not blank

            ret += "</component>" + Environment.NewLine;
			
            return ret;
        }


        /// <summary>
        /// Provides the ability to start a service.
        /// </summary>
        /// <param name="Title"></param>
        /// <returns>Status Message</returns>
        public string StartService()
        {
            if (_readonly) return "ERROR: Permission Denied!";
            if (_type != "service" && _machine != "" && _servicename != "") return "ERROR: MisConfiguration.";

            try
            {
                ServiceController sc = new ServiceController(_servicename, _machine);
                TimeSpan timeout = TimeSpan.FromMilliseconds(30000);  //WAIT 30 SECONDS
                sc.Start();
                sc.WaitForStatus(ServiceControllerStatus.Running, timeout);
                sc.Close();

                _status = "Running";
                return "Success: Running";
                
            }
            catch (Exception ex) { return "ERROR:" + ex.Message; }
        }

        /// <summary>
        /// Provides the ability to stop a service.
        /// </summary>
        /// <param name="Title"></param>
        /// <returns>Status Message</returns>
        public string StopService()
        {
            if (_readonly) return "ERROR: Permission Denied!";
            if (_type != "service" && _machine != "" && _servicename != "") return "ERROR: MisConfiguration.";

            try
            {
                ServiceController sc = new ServiceController(_servicename, _machine);
                TimeSpan timeout = TimeSpan.FromMilliseconds(30000);  //WAIT 30 SECONDS
                sc.Stop();
                sc.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                sc.Close();

                _status = "Stopped";
                return "Success: Stopped";
            }
            catch (Exception ex) { return "ERROR:" + ex.Message; }
        }

        
        


    }

    /// <summary>
    /// clsTimer contains a variety of functions that allow us to check the status of services, run relevance, etc...
    /// </summary>
    class clsTimer
    {
        public string GetServiceStatus(string ServiceName, string ServerName)
        {
            if (ServerName != "" && ServiceName != "")
            {
                string status = "ERROR:" + "Service " + ServiceName + " failed to return status on computer '" + ServerName + "'.";
                try
                {
                    ServiceController sc = new ServiceController(ServiceName, ServerName);
                    status = sc.Status.ToString();
                    sc.Close();
                }
                catch (Exception ex) { return "ERROR:" + ex.Message; }
                return status;
            }

            return "ERROR:" + "Configuration Failure... ServerName='" + ServerName + "'  ServiceName='" + ServiceName + "'.";
        }


        public string SetRegistryValue(string _Key, string _Value, string _Data)
        {
            if (_Key != "" && _Value != "")
            {
                try
                {
                    using (RegistryKey reg = Registry.LocalMachine.OpenSubKey(_Key, true))
                    {
                        try {
                            reg.SetValue(_Value, _Data);
                            return "Success";
                        }
                        catch (Exception ex) { return "ERROR:" + ex.Message; }
                    }
                }
                catch (Exception ex)
                {
                    return "ERROR:" + ex.Message;
                }
            }

            return "ERROR:" + "Configuration Failure... Key='" + _Key + "'  Value='" + _Value + "'.";
        }


        public string GetRegistryValue(string _Key, string _Value)
        {
            if (_Key != "" && _Value != "")
            {
                try
                {
                    using (RegistryKey reg = Registry.LocalMachine.OpenSubKey(_Key))
                    {
                        try { return reg.GetValue(_Value).ToString(); }
                        catch (Exception ex) { return "ERROR:" + ex.Message; }
                    }
                }
                catch (Exception ex)
                {
                    return "ERROR:" + ex.Message;
                }
            }

            return "ERROR:" + "Configuration Failure... Key='" + _Key + "'  Value='" + _Value + "'.";
        }





        //post value to Leftronic see https://www.leftronic.com/api for additional details.
        public string Leftronic_Post(string JSON)
        {
            string ret = "";

            // Create a request using a URL that can receive a post.             
            WebRequest request = WebRequest.Create("https://www.leftronic.com/customSend/");

            // Set the Method property of the request to POST.            
            request.Method = "POST";

            // Create POST data and convert it to a byte array.            
            //WRITE JSON DATA TO VARIABLE D            
            string postData = JSON; // "{\"accessKey\": \"nkLoNWqZ9u6ZzzQRJcBzo2NYn2GgPaCe\", \"streamName\": \"Infrastructure Number\", \"point\": " + textBox1.Text + " }";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.            
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.            
            request.ContentLength = byteArray.Length;
            // Get the request stream.            
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.            
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.            
            dataStream.Close();

            // Get the response.            
            WebResponse response = request.GetResponse();
            // Display the status.//            
            ret = ((HttpWebResponse)response).StatusDescription;
            // Get the stream containing content returned by the server.            
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.            
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.           
            string responseFromServer = reader.ReadToEnd();
            // Display the content.           
            ret += ":" + responseFromServer;
            // Clean up the streams.           
            reader.Close();
            dataStream.Close();
            response.Close();

            return ret;
        }



        public string[] WebReports_Post(string WRURL, string WRUsername, string WRPassword, string Relevance)
        {
            WebServiceInvoker invoker = new WebServiceInvoker(new Uri(WRURL));
 
            string service = "RelevanceBinding";
            string method = "GetRelevanceResult";
 
            string[] args = new string[] { Relevance, WRUsername, WRPassword };

            string[] ret = invoker.InvokeMethod<string[]>(service, method, args);
            
            return ret;
        }



        public string SQLServer_Post(string SqlCmd, string SqlServer, string SQLDatabase)
        {
            string ret = "";
            try
            {
                using (SqlConnection myConnection = new SqlConnection("Integrated Security=sspi;Database=" + SQLDatabase + ";Server=" + SqlServer + ";Connection Timeout=30"))
                {
                    myConnection.Open();

                    try
                    {
                        SqlDataReader myReader = null;
                        SqlCommand myCommand = new SqlCommand(SqlCmd, myConnection);
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            ret = myReader[0].ToString();
                        }
                    }
                    catch (SqlException sex)
                    {
                        ret = "SQL ERROR: " + sex.Message;
                    }
                    catch (Exception ex)
                    {
                        ret = "ERROR: " + ex.Message;
                    }

                }
            }
            catch (Exception ex)
            {
                ret = "POST ERROR: " + ex.Message;
            }
            return ret;
        }

    }



}
