//
//  BFAStatus.m
//  BigFix Admin
//
//  Created by Daniel Moran on 5/29/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import "BFAStatus.h"

@implementation BFAStatus

@synthesize title;
@synthesize status;
@synthesize type;
@synthesize category;

@synthesize data;


@synthesize variables;




-(NSString *) FindValue:(NSString *)varName {
    for (NSMutableArray *variable in variables) {
        if ([ [variable objectAtIndex:0] isEqualToString:varName]) {
            return [variable objectAtIndex:1];
        }
    }
    return @"";
}


-(void)AddValue:(NSString *)varName Value:(NSString *)varValue {
    NSArray *var = [NSArray arrayWithObjects:varName, varValue, nil];
    
    if (variables) {
        [variables addObject:var];
    }
    else {
        variables = [NSMutableArray arrayWithObjects:var, nil];
    }
}







@end



