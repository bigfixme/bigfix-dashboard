//
//  CmdParser.h
//  BigFix Admin
//
//  Created by Daniel Moran on 6/5/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CmdParser : NSObject <NSXMLParserDelegate>

@property (strong, nonatomic) NSMutableArray *results;

-(NSString *)submitCmd:(NSString *)cmd Arguments:(NSString *)args;


@end
