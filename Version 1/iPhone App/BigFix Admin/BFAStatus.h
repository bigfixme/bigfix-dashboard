//
//  BFAStatus.h
//  BigFix Admin
//
//  Created by Daniel Moran on 5/29/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BFAStatus : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *category;

@property (nonatomic, copy) NSString *data;

@property (nonatomic, strong) NSMutableArray *variables;

-(NSString *)FindValue:(NSString *)varName;
-(void)AddValue:(NSString *)varName Value:(NSString *)varValue;
@end
