//
//  main.m
//  BigFix Admin
//
//  Created by Daniel Moran on 5/25/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BFAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BFAAppDelegate class]));
    }
}
