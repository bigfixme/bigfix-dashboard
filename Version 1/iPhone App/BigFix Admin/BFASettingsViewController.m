//
//  BFASecondViewController.m
//  BigFix Admin
//
//  Created by Daniel Moran on 5/25/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import "BFASettingsViewController.h"



@interface BFASettingsViewController ()

@end

@implementation BFASettingsViewController
@synthesize Username;
@synthesize Password;
@synthesize txtServerURL;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"iPhoneGearBackground.jpg"]];
    
    
    //preload the users url into the text box if it's available
    //-----------------------------------
    //this code retrieves the global variable
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults) {
        self.txtServerURL.text = [standardUserDefaults objectForKey:@"usersServerURL"];
        self.Username.text = [standardUserDefaults objectForKey:@"usersUsername"];
        self.Password.text = [standardUserDefaults objectForKey:@"usersPassword"];
    }
    //-----------------------------------
    
        
    //if ([self connected])
    //{
    //    self.label.text = @"Inet Active";
    //}
    //else 
    //{
    //    self.label.text = @"Inet Not Active";
    //}
}

- (void)viewDidUnload
{
    [self setTxtServerURL:nil];
    [self setUsername:nil];
    [self setPassword:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}




//when the user hits DONE on the txtServerURL field... it will trigger this function
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if ((theTextField == self.txtServerURL) || (theTextField == self.Username) || (theTextField == self.Password)) {
        
        
        //-----------------------------------
        //this code saves the global variable
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        if (standardUserDefaults) {
            [standardUserDefaults setObject:self.txtServerURL.text forKey:@"usersServerURL"];
            [standardUserDefaults setObject:self.Username.text forKey:@"usersUsername"];
            [standardUserDefaults setObject:self.Password.text forKey:@"usersPassword"];
            
            [standardUserDefaults synchronize];
        }
        //-----------------------------------
        
    }
    
    [theTextField resignFirstResponder];
    return YES;
}




-(BOOL)connected
{
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL)
    {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) 
        {
            if ((flags & kSCNetworkFlagsReachable) == 0)
            {
                //if target host is not reachable
                return NO;
            }
            
            if ((flags & kSCNetworkFlagsConnectionRequired) == 0)
            {
                //if target host is reachable and no connection is required
                //then we'll assume (for now) that your on WiFi
                return YES;
            }
            
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs
                
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                return YES;
            }
        }
    }
    
    return NO;
}
@end















