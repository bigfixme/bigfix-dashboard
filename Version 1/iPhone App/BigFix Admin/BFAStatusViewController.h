//
//  BFAStatusViewController.h
//  BigFix Admin
//
//  Created by Daniel Moran on 5/29/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BFADetailViewController.h"

#import "PullRefreshTableViewController.h"

@interface BFAStatusViewController : PullRefreshTableViewController

@property (nonatomic, strong) NSMutableArray *status;



@end
