//
//  BFARelevanceViewController.m
//  BigFix Admin
//
//  Created by Daniel Moran on 5/26/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import "BFARelevanceViewController.h"

#import "CmdParser.h"

@interface BFARelevanceViewController () 

@end

@implementation BFARelevanceViewController
@synthesize tableView = _tableView;
@synthesize txtRelevance;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setTxtRelevance:nil];
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}










/// <summary>
/// When the user hits DONE on the txtServerURL field... it will trigger this function
/// </summary>
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.txtRelevance) {
        [theTextField resignFirstResponder];
    }
    
    if ([txtRelevance.text isEqualToString:@""]) { return YES; }
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    CmdParser *parser = [[CmdParser alloc] init];

    [listOfItems removeAllObjects];
        
    NSString *result = [parser submitCmd:@"relevance" Arguments:self.txtRelevance.text];
        
    if ([[result lowercaseString] rangeOfString:@"error"].location == NSNotFound) {
        //successful results... load into tableview list variable
        listOfItems = [parser.results mutableCopy];
    } else {
        //failure occured... tell user
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Relevance Statement Error" message:result delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
                
    //refresh the table view with the newly added data
    [[self tableView] reloadData];
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    return YES;
}




/// <summary>
/// This function is called by the system to return the number items to be displayed.
/// </summary>
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listOfItems count];
}


/// <summary>
/// This function is called by the system to return a particular cell.
/// </summary>
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *cellValue = [listOfItems objectAtIndex:indexPath.row];
    cell.textLabel.text = cellValue;
    
    return cell;
}






@end
