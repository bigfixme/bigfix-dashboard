//
//  BFASecondViewController.h
//  BigFix Admin
//
//  Created by Daniel Moran on 5/25/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>

@interface BFASettingsViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtServerURL;
@property (weak, nonatomic) IBOutlet UITextField *Username;
@property (weak, nonatomic) IBOutlet UITextField *Password;


@end

