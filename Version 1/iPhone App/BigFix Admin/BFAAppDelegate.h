//
//  BFAAppDelegate.h
//  BigFix Admin
//
//  Created by Daniel Moran on 5/25/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BFAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
