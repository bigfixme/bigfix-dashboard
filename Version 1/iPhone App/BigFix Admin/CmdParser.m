//
//  CmdParser.m
//  BigFix Admin
//
//  Created by Daniel Moran on 6/5/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import "CmdParser.h"

#import "BFAStatus.h"

@implementation CmdParser

@synthesize results;






-(NSString *)submitCmd:(NSString *)cmd Arguments:(NSString *)args {
    command = cmd; //save for xml parser use
    
    //-----------------------------------
    //this code retrieves the global variable
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *usersServerURL = nil;
    if (standardUserDefaults)
    {
        usersServerURL = [standardUserDefaults objectForKey:@"usersServerURL"];
    }
    //-----------------------------------
    
    if (usersServerURL != nil)
    {
        NSString *soapMsg = [self createCmdXML:cmd Arguments:args];
        
        NSURL *url = [NSURL URLWithString:usersServerURL];
        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    
        NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMsg length]];
    
        [req addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [req addValue:@"http://bigfix.me/admin/" forHTTPHeaderField:@"SOAPAction"];
        [req addValue:msgLength forHTTPHeaderField:@"Content-Length"];
        [req setHTTPMethod:@"POST"];
        [req setHTTPBody:[soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
    
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    
        if (error) {
            NSLog(@"Error sending soap request: %@", error);
            return @"ERROR: Please double check your Server URL on the settings tab.";// [NSString stringWithFormat:@"ERROR: %@", error];
        } else {
            //check to see if the server returned an error condition
            NSString *stringdata = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@", stringdata);
            
            if ([[stringdata lowercaseString] hasPrefix:@"error"]) {
                return stringdata;
            }
            else {
                NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:data];
        
                [xmlParser setDelegate:self];
                [xmlParser parse];
        
                //if this is a status command, we need to split up the results, sort and reassemble
                if ([[command lowercaseString] isEqualToString:@"status"]) {
                
                    //Parse XML results file
                    //Group XML Data into final array
                    //now organize based on type so they show up in the correct sub-categories
                    NSMutableArray *generalArray = [[NSMutableArray alloc] init];
                    NSMutableArray *serviceArray = [[NSMutableArray alloc] init];
                    NSMutableArray *otherArray = [[NSMutableArray alloc] init];
                
                    //sort results into appropriate array
                    for (BFAStatus *stat in results) {
                        if ([stat.category.lowercaseString isEqualToString:@"general"]) {
                            [generalArray addObject:stat];
                        }
                        else if ([stat.category.lowercaseString isEqualToString:@"service"]) {
                            [serviceArray addObject:stat];
                        }
                        else {
                            [otherArray addObject:stat];
                        }
                    }
                
                
                    //sort my services and other array
                    NSSortDescriptor *descriptors = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
                    [serviceArray sortUsingDescriptors:[NSArray arrayWithObject:descriptors]];
                    [otherArray sortUsingDescriptors:[NSArray arrayWithObject:descriptors]];
                
                    NSDictionary *generalDict = [NSDictionary dictionaryWithObject:generalArray forKey:@"General"];
                    NSDictionary *serviceDict = [NSDictionary dictionaryWithObject:serviceArray forKey:@"Service"];
                    NSDictionary *otherDict = [NSDictionary dictionaryWithObject:otherArray forKey:@"Other"];
                
                    
                    //now reassemble back into the results array
                    [results removeAllObjects];
                    results = [[NSMutableArray alloc] init];
                
                    [results addObject:generalDict];
                    [results addObject:serviceDict];
                    [results addObject:otherDict];
                
                    return @"Success";
                    
                }
                else {
                    //cmd result... confirm it doesn't contain an error in the first value
                    if ([[[results objectAtIndex:0] lowercaseString] hasPrefix:@"error"]) {
                        return [results objectAtIndex:0];
                    }
                    else {
                        return @"Success";
                    }
                }
                                
            }
        }

    } 
    else {
        return @"ERROR:  Please configure the Server URL on the settings tab.";
    }
    
}




-(NSString *)createCmdXML:(NSString *)cmd Arguments:(NSString *)args {

    //-----------------------------------
    //this code retrieves the global variable
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *usersServerURL = nil;
    NSString *usersUsername = nil;
    NSString *usersPassword = nil;
    if (standardUserDefaults)
    {
        usersServerURL = [standardUserDefaults objectForKey:@"usersServerURL"];
        usersUsername = [standardUserDefaults objectForKey:@"usersUsername"];
        usersPassword = [standardUserDefaults objectForKey:@"usersPassword"];
    }
    //-----------------------------------


    //===============================================================
    //start with header of soap message
    NSString *soapMsg = @"<?xml version=\"1.0\" encoding=\"utf-8\"?><env:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\"><env:Body>";

    //add my relevance/user/pass section of the soap message
    soapMsg = [soapMsg stringByAppendingString:[NSString stringWithFormat:@"<Command action=\"%@\" arguments=\"%@\" />",[self EscapeString:cmd], [self EscapeString:args]]];

    if ((usersUsername != nil) && (usersPassword != nil)) {
        soapMsg = [soapMsg stringByAppendingString:[NSString stringWithFormat:@"<Credentials username=\"%@\" password=\"%@\" />", usersUsername, usersPassword]];
    }

    //now wrap up the soap message
    soapMsg = [soapMsg stringByAppendingString:@"</env:Body></env:Envelope>"];
    
    return soapMsg;
}



-(NSString *)EscapeString:(NSString *)str {
    str = [str stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    str = [str stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
    str = [str stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"];
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
    
    return str;
}










NSString *command = @"";




-(void)parserDidStartDocument:(NSXMLParser *)parser {
    //the parser started this document.
    
    results = [[NSMutableArray alloc] init];    
}

NSString *tagName = @"";

-(void) parser:(NSXMLParser *)parser 
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName
    attributes:(NSDictionary *)attributeDict {
    
    tagName = elementName;  //this saves the tag name so we can process it later
    
    if ([[command lowercaseString] isEqualToString:@"status"]) {
        if ([elementName isEqualToString:@"component"]) {
            //if the element tag had sub-properties, i can pull them out as follows:
            //NSString *subitem = [attributeDict objectForKey:@"subitem"];
            
            BFAStatus *stat = [[BFAStatus alloc] init];
            stat.title = [attributeDict objectForKey:@"title"];
            stat.type = [attributeDict objectForKey:@"type"];
            stat.status = [attributeDict objectForKey:@"status"];
            stat.category = [attributeDict objectForKey:@"category"];
            
            stat.variables = [[NSMutableArray alloc] init];
            
            [results addObject:stat];
        }

    }
    else {
        if ([elementName isEqual:@"a"]) {
            //if the element tag had sub-properties, i can pull them out as follows:
            //NSString *subitem = [attributeDict objectForKey:@"subitem"];
        }
    }
}

//this function lets me "grab" the "inner text" of any tag
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([[command lowercaseString] isEqualToString:@"status"]) {
        //pull a pointer to the last item in the array so we can add data
        BFAStatus *stat = [results lastObject];
        
        if ([tagName isEqualToString:@"component"]) {
            stat.data = string;
        }
        else {
            [stat AddValue:tagName Value:string];
        }
    }
    else {
    
        if ([tagName isEqualToString:@"a"]) {
            //results = [NSString stringWithFormat:@"%@", string];
            [results addObject:string];
        }
    }
    
    tagName = @"";
}


-(void)parserDidEndDocument:(NSXMLParser *)parser {
    //the parser finished.
}

@end
