//
//  BFADetailViewController.m
//  BigFix Admin
//
//  Created by Daniel Moran on 5/29/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import "BFADetailViewController.h"

#import "BFAStatus.h"
#import "CmdParser.h"

@interface BFADetailViewController ()
- (void)configureView;
@end

@implementation BFADetailViewController 

@synthesize lblTitle;
@synthesize swtStatus;
@synthesize detailItem = _detailItem;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    activityInd.hidden = YES;
    
    [self configureView];
}

- (void)viewDidUnload
{
    [self setLblTitle:nil];
    [self setSwtStatus:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}






- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



















/// <summary>
/// Visually configure the form based on what type of object this is.
/// </summary>
- (void)configureView {
    if (self.detailItem) {
        BFAStatus *stat = self.detailItem;
        
        self.navigationItem.title = [NSString stringWithFormat:@"%@ Details", stat.type];
        self.lblTitle.text = stat.title;
                
        
        //now it's time to display or hide things based on what type of status item this is.
        if ([stat.type.lowercaseString isEqualToString:@"service"]) {
            /// Service Details Screen Layout
            
            //----------------------
            //STATUS
                        
            
            if ([ [stat FindValue:@"readonly"] isEqualToString:@"False"]) {
                swtStatus.enabled = YES;
            } else {
                swtStatus.enabled = NO;
                UILabel *substatusLabel = [self AddLabel:@"Read Only" Rect:CGRectMake(210, 40, 75, 23)];        
                substatusLabel.font = [UIFont systemFontOfSize:11.0];
                substatusLabel.textAlignment = UITextAlignmentRight;
                substatusLabel.textColor = [UIColor grayColor];
            }
            
            if ([stat.status isEqualToString:@"Running"] || [stat.status isEqualToString:@"Stopped"]) {
                swtStatus.hidden = NO;
                if ([stat.status isEqualToString:@"Running"])
                    swtStatus.on = YES;
                else swtStatus.on = NO;
                [swtStatus addTarget:self action:@selector(switchTouched:) forControlEvents:UIControlEventValueChanged];  //ADDS CALL TO MY FUNCTION WHEN SWITCH IS MANIPULATED
            }
            else {
                //add a label with "Unknown"
                UILabel *statusLabel = [self AddLabel:stat.status Rect:CGRectMake(175, 20, 114, 23)];        
                statusLabel.font = [UIFont systemFontOfSize:15.0];
                statusLabel.textAlignment = UITextAlignmentRight;
                if ([stat.status isEqualToString:@"ERROR"]) statusLabel.textColor = [UIColor redColor];
                
                swtStatus.hidden = YES;
            }
            
            //----------------------
            //DATA label
            UILabel *dataLabel = [self AddLabel:stat.data Rect:CGRectMake(20, 63, 280, 200)];  
            //dataLabel.font = [UIFont systemFontOfSize:13.0];
            dataLabel.lineBreakMode = UILineBreakModeWordWrap;
            dataLabel.numberOfLines = 12;
            
        }
        else {
            /// Generic Details Screen Layout
            
            //----------------------
            //STATUS
            UILabel *statusLabel = [self AddLabel:stat.status Rect:CGRectMake(175, 20, 130, 23)];        
            statusLabel.font = [UIFont systemFontOfSize:13.0];
            statusLabel.textAlignment = UITextAlignmentRight;
            swtStatus.hidden = YES;
            
            //----------------------
            //DATA label
            UILabel *dataLabel = [self AddLabel:stat.data Rect:CGRectMake(20, 63, 280, 200)];        
            dataLabel.lineBreakMode = UILineBreakModeWordWrap;
            dataLabel.numberOfLines = 12; 
        }
        
    }
}


/// <summary>
/// This function adds the UILabel to the form.
/// </summary>
-(UILabel *)AddLabel:(NSString *)text Rect:(CGRect)rect {
    UILabel *lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.text = text;
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textAlignment = UITextAlignmentLeft;
    
    [self.view addSubview:lbl];
    return lbl;
}




/// <summary>
/// User just tapped the on/off switch and this function responds by kicking off the updateService function
/// </summary>
-(void)switchTouched:(UIControl *)sender {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    activityInd.hidden = NO;
    swtStatus.hidden = YES;
    
    [activityInd startAnimating];
        
    [self performSelector:@selector(updateService)
               withObject:nil
               afterDelay: 0];
}

/// <summary>
/// This function reads the new state of the on/off switch and responds by issuing a start or stop command for this service.
/// </summary>
-(void)updateService {
    BFAStatus *stat = self.detailItem;
    
    
    CmdParser *parser = [[CmdParser alloc] init];
    
    if (swtStatus.on) 
    {
        //self.lblTitle.text = @"on";
        NSString *result = [parser submitCmd:@"servicestart" Arguments:[NSString stringWithFormat:@"%@", stat.title]];
        
        if ([[result lowercaseString] rangeOfString:@"error"].location == NSNotFound) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Service Successfully Started" message:[parser.results objectAtIndex:0] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            stat.status = @"Running"; //update status since cmd succeeded
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Service Failed to Start" message:[parser.results objectAtIndex:0] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];                
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            swtStatus.on = NO; //push control back to on position since the cmd failed
            [UIView commitAnimations];
        }
    } else {
        //self.lblTitle.text = @"off";
        NSString *result = [parser submitCmd:@"servicestop" Arguments:[NSString stringWithFormat:@"%@", stat.title]];
        
        if ([[result lowercaseString] rangeOfString:@"error"].location == NSNotFound) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Service Successfully Stopped" message:[parser.results objectAtIndex:0] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            stat.status = @"Stopped";  //update status since cmd succeeded
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Service Failed to Stop" message:[parser.results objectAtIndex:0] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            swtStatus.on = YES; //push control back to off position since the cmd failed
            [UIView commitAnimations];
            
            
        }
    }
    
    [activityInd stopAnimating];
    
    activityInd.hidden = YES;
    swtStatus.hidden = NO;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

}





@end
