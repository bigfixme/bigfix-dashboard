//
//  BFARelevanceViewController.h
//  BigFix Admin
//
//  Created by Daniel Moran on 5/26/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BFARelevanceViewController : UIViewController <UITextFieldDelegate> {
    NSMutableArray *listOfItems;
}




@property (weak, nonatomic) IBOutlet UITextField *txtRelevance;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
