//
//  BFADetailViewController.h
//  BigFix Admin
//
//  Created by Daniel Moran on 5/29/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BFADetailViewController : UIViewController {
    
    IBOutlet UIActivityIndicatorView *activityInd;
    NSTimer *timer;
    
}


@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UISwitch *swtStatus;

@property (strong, nonatomic) id detailItem;





@end
