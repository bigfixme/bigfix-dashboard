//
//  BFAStatusViewController.m
//  BigFix Admin
//
//  Created by Daniel Moran on 5/29/12.
//  Copyright (c) 2012 DanielHeth. All rights reserved.
//

#import "BFAStatusViewController.h"

#import "BFAStatus.h"
#import "BFADetailViewController.h"
#import "CmdParser.h"

@interface BFAStatusViewController ()

@end

@implementation BFAStatusViewController {
    NSMutableArray *status;
}


@synthesize status;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    //self.clearsSelectionOnViewWillAppear = YES;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //[self refresh];
}

-(void)viewWillAppear:(BOOL)animated {
    [[self tableView] reloadData];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    //return 1;
    
    return [status count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [status count];

    NSDictionary *dictionary = [status objectAtIndex:section];
    NSArray *keys = [dictionary allKeys];
    
    NSMutableArray *array = [dictionary objectForKey:[keys objectAtIndex:0]];
    return [array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StatusCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] init];
    }
    
    //pull correct data from array
    NSDictionary *dictionary = [status objectAtIndex:indexPath.section];
    NSArray *keys = [dictionary allKeys];
    NSMutableArray *array = [dictionary objectForKey:[keys objectAtIndex:0]];
    
    BFAStatus *stat = [array objectAtIndex:indexPath.row];
    //BFAStatus *stat = [status objectAtIndex:indexPath.row];
    cell.textLabel.text = stat.title;
    cell.detailTextLabel.text = stat.status;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


-(void)refresh {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [self performSelector:@selector(refreshBackEnd)
               withObject:nil
               afterDelay: 0];
    
    
}

-(void)refreshBackEnd {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    CmdParser *parser = [[CmdParser alloc] init];
    
    [status removeAllObjects];
    
    NSString *result = [parser submitCmd:@"status" Arguments:@""];
    
    if ([[result lowercaseString] rangeOfString:@"error"].location == NSNotFound) {
        //successful results... load into tableview list variable
        status = [parser.results mutableCopy];
    } else {
        //failure occured... tell user
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status Error" message:result delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:1.0];
    
    //refresh the table view with the newly added data
    [[self tableView] reloadData];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    
    
    
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self performSegueWithIdentifier:@"segueToDetails" sender:self];
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSDictionary *dictionary = [status objectAtIndex:section];
    NSArray *keys = [dictionary allKeys];
    return [keys objectAtIndex:0];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
    
    NSDictionary *dictionary = [status objectAtIndex:selectedIndexPath.section];
    NSArray *keys = [dictionary allKeys];
    NSMutableArray *array = [dictionary objectForKey:[keys objectAtIndex:0]];
    
    BFAStatus *stat = [array objectAtIndex:selectedIndexPath.row];
    //BFAStatus *stat = (BFAStatus *) [status objectAtIndex:rowNumber];
    
    BFADetailViewController *details = [segue destinationViewController];
    details.detailItem = stat;
    
    
}

















//======================================================================================================================

//======================================================================================================================



@end




